package com.example.kym.ellie;

//--------------------------Holds the code for showing the per device consumption--------------------------------


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Home_Ellie_Devices extends Fragment implements Home.YourFragmentInterface {

    protected boolean onCreateViewCalled = false;
    TextView title;
    ArrayList<dev_cons>Devices_Cons =  new ArrayList<dev_cons>();
    float total_elec_cons = 0;
    ListView dev_cons_list;
    DrawerListAdapter adapter;

//--------------------------Create dev cons object--------------------------------

    public static class dev_cons{
        String name, app_type;
        int dev_id;
        float elec_cons;

        public dev_cons(String Name, String App_Type, int Dev_ID, float Elec_Cons){
            name = Name;
            app_type = App_Type;
            dev_id = Dev_ID;
            elec_cons = Elec_Cons;
        }
    }

    int[] Colors={
            Color.parseColor("#F44336"),
            Color.parseColor("#4CAF50"),
            Color.parseColor("#2196F3"),
            Color.parseColor("#FF5722")
    };

    //--------------------------Creates the list and makes the function of each element--------------------------------

    class DrawerListAdapter extends BaseAdapter {

        Context contexT;
        ArrayList<dev_cons> devices_array;

        public DrawerListAdapter(Context context, ArrayList<dev_cons> DEV_CONS) {
            contexT = context;
            devices_array = DEV_CONS;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public int getCount() {
            return devices_array.size();
        }

        @Override
        public Object getItem(int position) {
            return devices_array.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) contexT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.dev_cons_item, null);
            }
            else {
                view = convertView;
            }

            TextView circle = (TextView)view.findViewById(R.id.icon);
            ImageView icon = (ImageView)view.findViewById(R.id.icon_img);
            TextView name = (TextView)view.findViewById(R.id.name);
            TextView infos = (TextView)view.findViewById(R.id.infos);
            RelativeLayout shaded = (RelativeLayout)view.findViewById(R.id.shaded);
            TextView percentage = (TextView)view.findViewById(R.id.percentage);

            LayerDrawable bg = (LayerDrawable)circle.getBackground();
            GradientDrawable circle_icon = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);

            circle_icon.setColor(Colors[position % 4]);

            name.setText(devices_array.get(position).name);
            icon.setImageDrawable(getResources().getDrawable(application_types.get_icon(devices_array.get(position).app_type)));
            infos.setText(String.format("₱%.2f", devices_array.get(position).elec_cons));
            percentage.setText(String.format("%.2f", devices_array.get(position).elec_cons*100/total_elec_cons)+"%");
            shaded.setTranslationX(-750f);
            shaded.animate().translationX(((devices_array.get(position).elec_cons/total_elec_cons)-1)*750f).setDuration(1500).start();

            return view;
        }
    }

    //--------------------------Fetches the consumptions--------------------------------

    private void fetch_cons(){
        for(int i = 0; i < user_info.init_dev_cons.size(); i++){
            Devices_Cons.add(user_info.init_dev_cons.get(i));
        }
        total_elec_cons = user_info.gettotaldevcons();
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    public void fragmentBecameVisible() {
        if(onCreateViewCalled) {
            title.setTranslationY(-150f);
            title.animate().translationY(0f).setDuration(750);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home__ellie__devices, container, false);
        title = (TextView)v.findViewById(R.id.home_devices_section_title);
        title.setTranslationY(-150f);
        title.animate().translationY(0f).setDuration(750);

        fetch_cons();

        dev_cons_list = (ListView)v.findViewById(R.id.dev_cons_list);
        adapter = new DrawerListAdapter(getActivity(), Devices_Cons);

        dev_cons_list.setAdapter(adapter);

        onCreateViewCalled = true;
        return v;
    }

}
