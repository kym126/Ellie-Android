package com.example.kym.ellie;

//--------------------------Holds the code for the Devices Fragment(Page)--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

public class Devices extends Fragment {

    ListView device_list;
    public static ArrayList<Device> Devices = new ArrayList<Device>();
    NetworkHelper networkHelper = new NetworkHelper();
    int num_list = 0;
    private static final int create_dev = 26, edit_dev = 21;
    DrawerListAdapter adapter;
    boolean has_added = false, is_animating = false;
    View clicked_view;
    ArrayList<View> list = new ArrayList<View>();

    ArrayList<Integer> QR_codes = new ArrayList<Integer>();

    int[] Colors={
            Color.parseColor("#F44336"),
            Color.parseColor("#4CAF50"),
            Color.parseColor("#2196F3"),
            Color.parseColor("#FF5722")
    };

    TextView no_dev;

    //------------------------------------Creates an object "Device"--------------------------------

    public static class Device {
        String Name;
        String Brand;
        String Desc;
        String App_Type;
        String Loc;
        int device_id;
        int device_code;
        int mIcon;
        boolean Active;

        public Device(String Object, String desc, String app_type, String brand, String loc, int Icon, int id, int code, boolean active) {
            Name = Object;
            Brand = brand;
            App_Type = app_type;
            Desc = desc;
            Loc = loc;
            mIcon = Icon;
            device_id = id;
            device_code = code;
            Active = active;
        }
    }

    //-----------------------------Creates the functionalities of the list of devices--------------------------------

    class DrawerListAdapter extends BaseAdapter {

        Context Context;
        ArrayList<Device> devices_array;

        public DrawerListAdapter(Context context, ArrayList<Device> devices) {
            Context = context;
            devices_array = devices;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            if (is_animating){
                return false;
            }else{
                return true;
            }
        }

        @Override
        public int getCount() {
            return devices_array.size();
        }

        @Override
        public Object getItem(int position) {
            return devices_array.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.devices_item, null);
            }
            else {
                view = convertView;
            }

            TextView objectView = (TextView) view.findViewById(R.id.object);
            TextView brandView = (TextView) view.findViewById(R.id.brand);
            ImageView iconView = (ImageView) view.findViewById(R.id.icon);
            ImageView iconimg = (ImageView) view.findViewById(R.id.icon_img);
            TextView status = (TextView) view.findViewById(R.id.status);

            LayerDrawable circle = (LayerDrawable)iconView.getBackground();
            GradientDrawable circle_icon = (GradientDrawable)circle.findDrawableByLayerId(R.id.circle_bg);

            circle = (LayerDrawable)view.getBackground();
            GradientDrawable view_bg= (GradientDrawable)circle.findDrawableByLayerId(R.id.section_bg);

            circle_icon.setColor(Colors[position % 4]);

            if(position == 0){
                list.clear();
            }

            list.add(view);

            objectView.setText(devices_array.get(position).Name);
            brandView.setText(devices_array.get(position).Brand);
            iconimg.setImageResource(devices_array.get(position).mIcon);
            if(!devices_array.get(position).Active){
                status.setVisibility(View.INVISIBLE);
            }

            if(device_list.getCount() - 1 == position&&has_added) {
                view_bg.setStroke(0, getResources().getColor(R.color.white_teal));
                view.setVisibility(View.INVISIBLE);
                has_added = false;
                final Handler delay = new Handler();
                delay.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(View.VISIBLE);
                        Animation add = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
                        add.setDuration(500);
                        view.startAnimation(add);
                        final Handler delay1 = new Handler();
                        delay1.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                is_animating = false;
                            }
                        }, 700);
                    }
                }, 700);
            }else if(device_list.getCount() - 1 == position){
                view_bg.setStroke(0, getResources().getColor(R.color.white_teal));
                is_animating = false;
            }else{
                view_bg.setStroke(2, Color.parseColor("#C5D5D4"));
            }
            return view;
        }
    }

    //-----------------------------Returns the device id based from the name given--------------------------------

    public static int get_dev_id(String dev_name){
        for(int i = 0; i < Devices.size(); i++) {
            if (Devices.get(i).Name.matches(dev_name)) {
                return Devices.get(i).device_id;
            }
        }
        return 0;
    }

    //---------------------------Returns device name based from device id given--------------------------------

    public static String get_dev_name(int dev_id){
        for(int i = 0; i < Devices.size(); i++) {
            if (Devices.get(i).device_id == dev_id) {
                return Devices.get(i).Name;
            }
        }
        return "";
    }

    //------------------------------------Creates error dialog--------------------------------

    private void show_error() {

        AlertDialog.Builder save_device = new AlertDialog.Builder(getActivity());
        save_device.setTitle("QR Code Error");
        save_device.setMessage("Please make sure that the QR code you are scanning is the one attached to the smartplug.");
        save_device.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog helpDialog = save_device.create();
        helpDialog.show();
    }

    //------------------------------------Creates failed dialog--------------------------------

    private void show_failed() {

        AlertDialog.Builder save_device = new AlertDialog.Builder(getActivity());
        save_device.setTitle("QR Code Error");
        save_device.setMessage("You cannot register the same smartplug twice.");
        save_device.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog helpDialog = save_device.create();
        helpDialog.show();
    }

    //------------------------------------Update the devices on the list--------------------------------

    public void populate_list(){
        if (user_info.init_devices.size() == 0) {
            no_dev.setVisibility(View.VISIBLE);
        } else {
            no_dev.setVisibility(View.INVISIBLE);
            for (int i = 0; i < user_info.init_devices.size(); i++) {
                Devices.add(user_info.init_devices.get(i));
                QR_codes.add(user_info.init_devices.get(i).device_code);
            }
        }
    }

    //------------------------------------Handles the different activites that may return to this page--------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {

            //result from qr code scan

            case(49374):
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                try{
                    if (result != null){
                        if (result.getContents() == null) {
                        } else {
                            int i, code = Integer.parseInt(result.getContents());;
                            for(i = 0; i < QR_codes.size(); i++){
                                if(QR_codes.get(i) == code){
                                    break;
                                }
                            }
                            if(i == QR_codes.size()) {
                                Intent create_device = new Intent(getActivity(), edit_device.class);
                                create_device.putExtra("device_id", 0);
                                create_device.putExtra("device_code", code);
                                create_device.putExtra("list_pos", Devices.size());
                                startActivityForResult(create_device, create_dev);
                            }else{
                                show_failed();
                            }
                        }
                    } else {
                        // This is important, otherwise the result will not be passed to the fragment
                        super.onActivityResult(requestCode, resultCode, data);
                    }
                }catch (NumberFormatException e){
                    show_error();
                }

                break;

            //if activity return was from create device

            case(create_dev):
                if (resultCode == Activity.RESULT_OK&&data.getBooleanExtra("changed",false) == true) {
                    is_animating = true;
                    String new_dev_name = data.getStringExtra("device_name");
                    String new_dev_desc = data.getStringExtra("device_desc");
                    String new_dev_app_type = data.getStringExtra("device_app_type");
                    String new_dev_brand = data.getStringExtra("device_brand");
                    String new_dev_loc = data.getStringExtra("device_loc");
                    int new_dev_id = data.getIntExtra("device_id", 0);
                    int new_dev_code = data.getIntExtra("device_code", 0);
                    QR_codes.add(new_dev_code);
                    Devices.add(new Device(new_dev_name, new_dev_desc, new_dev_app_type, new_dev_brand, new_dev_loc, application_types.get_icon(new_dev_app_type), new_dev_id, new_dev_code, false));
                    has_added = true;
                    adapter.notifyDataSetChanged();
                    final Handler delay = new Handler();
                    delay.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            device_list.smoothScrollToPositionFromTop(device_list.getCount(), 0, 500);
                        }
                    }, 700);
                    if (num_list == 0) {
                        no_dev.setVisibility(View.INVISIBLE);
                    }
                    num_list++;
                }
                break;

            //if activity return was from edit/delete device

            case(edit_dev):
                if (resultCode == Activity.RESULT_OK&&data.getBooleanExtra("deleted",false) == true) {
                    is_animating = true;
                    final int position = data.getIntExtra("list_pos", 0);
                    QR_codes.remove(position);
                    final Handler delay1 = new Handler();
                    delay1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Animation fade = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);
                            fade.setDuration(700);
                            device_list.getChildAt(position).setVisibility(View.INVISIBLE);
                            device_list.getChildAt(position).startAnimation(fade);
                            final Handler delay2 = new Handler();
                            delay2.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    for(int i = position; i < list.size(); i++){
                                        list.get(i).animate().translationY(-200f).setDuration(500).start();
                                    }
                                    if (Devices.size() == 1) {
                                        no_dev.setVisibility(View.VISIBLE);
                                    }
                                    if(position == (list.size() - 1)){
                                        Devices.remove(position);
                                        adapter.notifyDataSetChanged();
                                        device_list.getChildAt(position).setVisibility(View.VISIBLE);
                                    }
                                    num_list--;
                                    final Handler delay3 = new Handler();
                                    delay3.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            for(int i = position; i < list.size(); i++){
                                                list.get(i).setTranslationY(0f);
                                            }
                                            if(position != list.size()){
                                                Devices.remove(position);
                                                adapter.notifyDataSetChanged();
                                                device_list.getChildAt(position).setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }, 700);
                                }
                            }, 1200);
                        }
                    }, 700);
                }else if(resultCode == Activity.RESULT_OK&&data.getBooleanExtra("changed",false) == true){
                    is_animating = true;
                    final int position = data.getIntExtra("list_pos", 0);
                    final String new_dev_name = data.getStringExtra("device_name");
                    final String new_dev_desc = data.getStringExtra("device_desc");
                    final String new_dev_app_type = data.getStringExtra("device_app_type");
                    final String new_dev_brand = data.getStringExtra("device_brand");
                    final String new_dev_loc = data.getStringExtra("device_loc");
                    final Boolean new_active = data.getBooleanExtra("active", false);

                    final Animation fade = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
                    fade.setDuration(500);

                    final Animation fade_in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
                    fade_in.setDuration(500);

                    int changed = 0;
                    if(!Devices.get(position).Name.matches(new_dev_name)){
                        changed = changed | 1;
                    }
                    if(!Devices.get(position).Brand.matches(new_dev_brand)){
                        changed = changed | 2;
                    }
                    if(!Devices.get(position).App_Type.matches(new_dev_app_type)){
                        changed = changed | 4;
                    }
                    if(new_active != Devices.get(position).Active){
                        changed = changed | 16;
                    }

                    if(changed != 0){
                        final TextView title = (TextView)clicked_view.findViewById(R.id.object);
                        final TextView brandView = (TextView)clicked_view.findViewById(R.id.brand);
                        final ImageView iconimg = (ImageView)clicked_view.findViewById(R.id.icon_img);
                        final TextView status = (TextView)clicked_view.findViewById(R.id.status);

                        final int change_val = changed;
                        final Handler delay1 = new Handler();
                        final int icon = application_types.get_icon(new_dev_app_type);

                        Devices.get(position).Name = new_dev_name;
                        Devices.get(position).Desc = new_dev_desc;
                        Devices.get(position).App_Type = new_dev_app_type;
                        Devices.get(position).Brand = new_dev_brand;
                        Devices.get(position).Loc = new_dev_loc;
                        Devices.get(position).Active = new_active;
                        Devices.get(position).mIcon = icon;

                        delay1.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if((change_val&1) != 0){
                                    title.startAnimation(fade);
                                    title.setVisibility(View.INVISIBLE);
                                }

                                if((change_val&2) != 0){
                                    brandView.startAnimation(fade);
                                    brandView.setVisibility(View.INVISIBLE);
                                }

                                if((change_val&4) != 0){
                                    iconimg.startAnimation(fade);
                                    iconimg.setVisibility(View.INVISIBLE);
                                }

                                if((change_val&16) != 0){
                                    if(new_active == true){
                                        status.startAnimation(fade_in);
                                        status.setVisibility(View.VISIBLE);
                                    }else{
                                        status.startAnimation(fade);
                                        final Handler status_delay = new Handler();
                                        status_delay.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                status.setVisibility(View.INVISIBLE);
                                            }
                                        }, 500);
                                    }
                                }

                                final Handler delay2 = new Handler();
                                delay2.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        title.setText(new_dev_name);
                                        brandView.setText(new_dev_brand);
                                        iconimg.setImageDrawable(getResources().getDrawable(icon));

                                        if((change_val&1) != 0){
                                            title.startAnimation(fade_in);
                                            title.setVisibility(View.VISIBLE);
                                        }

                                        if((change_val&2) != 0){
                                            brandView.startAnimation(fade_in);
                                            brandView.setVisibility(View.VISIBLE);
                                        }

                                        if((change_val&4) != 0){
                                            iconimg.startAnimation(fade_in);
                                            iconimg.setVisibility(View.VISIBLE);
                                        }

                                        final Handler delay3 = new Handler();
                                        delay3.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                adapter.notifyDataSetChanged();
                                            }
                                        }, 800);
                                    }
                                }, 800);
                            }
                        }, 700);

                    }else {
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.devices,container,false);

        no_dev = (TextView) v.findViewById(R.id.no_devices);

        Devices.clear();

        device_list=(ListView)v.findViewById(R.id.list_devices);
        adapter = new DrawerListAdapter(getActivity(), Devices);

        device_list.setAdapter(adapter);

        populate_list();

        final FloatingActionButton add_device= (FloatingActionButton)v.findViewById(R.id.add_device);

        final IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
        integrator.setPrompt("Scan the QR code of the smart plug");
        integrator.setCameraId(Camera.CameraInfo.CAMERA_FACING_BACK);
        integrator.setCaptureActivity(QR_scanner.class).setOrientationLocked(false);

        add_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });

        device_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clicked_view = view;
                adapter.notifyDataSetChanged();
                Intent create_device = new Intent(getActivity(), edit_device.class);
                create_device.putExtra("device_id", Devices.get(position).device_id);
                create_device.putExtra("device_code", Devices.get(position).device_code);
                create_device.putExtra("device_name", Devices.get(position).Name);
                create_device.putExtra("device_desc", Devices.get(position).Desc);
                create_device.putExtra("device_app_type", Devices.get(position).App_Type);
                create_device.putExtra("device_brand", Devices.get(position).Brand);
                create_device.putExtra("device_loc", Devices.get(position).Loc);
                create_device.putExtra("active", Devices.get(position).Active);
                create_device.putExtra("list_pos", position);
                startActivityForResult(create_device, edit_dev);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        return v;
    }
}