package com.example.kym.ellie;

//--------------------------Holds the code for the home page and seperates it into 3 fragments--------------------------------
//--------------------------Do not edit only for cosmetics--------------------------------


import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Home extends Fragment {

    //Context context = getActivity();

    public interface YourFragmentInterface {
        void fragmentBecameVisible();
    }

    private class HomePagerAdapter extends FragmentPagerAdapter {

        public HomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return new Home_Ellie_Monthly();
                case 1: return new Home_Ellie_Devices();
                case 2: return new Home_Ellie_Trend();
                default: return new Home_Ellie_Trend();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    public void change_view(TextView active, TextView passive_1, TextView passive_2, GradientDrawable active_dwbl, GradientDrawable passive1_dwbl, GradientDrawable passive2_dwbl){
        active.setTextColor(Color.rgb(224, 242, 241));
        passive_1.setTextColor(Color.rgb(0, 121, 107));
        passive_2.setTextColor(Color.rgb(0, 121, 107));
        active_dwbl.setColor(Color.rgb(0, 121, 107));
        passive1_dwbl.setColor(Color.rgb(224, 242, 241));
        passive2_dwbl.setColor(Color.rgb(224, 242, 241));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home, container, false);
        final TextView Monthly = (TextView)v.findViewById(R.id.monthly_word);
        final TextView Devices = (TextView)v.findViewById(R.id.devices_word);
        final TextView Trend = (TextView)v.findViewById(R.id.trend_word);

        // Assigning ViewPager View and setting the adapter
        final ViewPager pager = (ViewPager)v.findViewById(R.id.home_contents);
        final HomePagerAdapter adapter = new HomePagerAdapter(getFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(3);
        pager.setCurrentItem(0);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                YourFragmentInterface fragment = (YourFragmentInterface) adapter.instantiateItem(pager, position);
                if (fragment != null) {
                    fragment.fragmentBecameVisible();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        LayerDrawable monthly_drawable = (LayerDrawable)Monthly.getBackground();
        final GradientDrawable monthly_btn = (GradientDrawable) monthly_drawable.findDrawableByLayerId(R.id.Monthly_Button);
        LayerDrawable devices_drawable = (LayerDrawable)Devices.getBackground();
        final GradientDrawable devices_btn = (GradientDrawable) devices_drawable.findDrawableByLayerId(R.id.Devices_Button);
        LayerDrawable trend_drawable = (LayerDrawable)Trend.getBackground();
        final GradientDrawable trend_btn = (GradientDrawable) trend_drawable.findDrawableByLayerId(R.id.Trend_Button);

        change_view(Monthly, Devices, Trend, monthly_btn, devices_btn, trend_btn);

        Monthly.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (Monthly.getCurrentTextColor() != Color.rgb(224, 242, 241)) {
                            change_view(Monthly, Devices, Trend, monthly_btn, devices_btn, trend_btn);
                            pager.setCurrentItem(0);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;
            }
        });

        Trend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (Trend.getCurrentTextColor() != Color.rgb(224, 242, 241)) {
                            change_view(Trend, Devices, Monthly, trend_btn, devices_btn, monthly_btn);
                            pager.setCurrentItem(2);
                            break;
                        }
                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;
            }
        });

        Devices.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (Devices.getCurrentTextColor() != Color.rgb(224, 242, 241)) {
                            change_view(Devices, Monthly, Trend, devices_btn, monthly_btn, trend_btn);
                            pager.setCurrentItem(1);
                            break;
                        }
                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;
            }
        });
        return v;
    }
}