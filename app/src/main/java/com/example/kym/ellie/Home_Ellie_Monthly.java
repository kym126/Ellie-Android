package com.example.kym.ellie;

//--------------------------Holds the code for displaying the stats for current month--------------------------------

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.graphics.Color;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.Calendar;

public class Home_Ellie_Monthly extends Fragment implements Home.YourFragmentInterface{

    protected boolean onCreateViewCalled = false;
    TextView title, cons, overflow;
    EditText quota_val;
    boolean is_overflow = false;

    private PieChart mChart;

    float consumption [] = {22, 78};
    String xvalues [] = {"", ""};
    int[] chartColor = {Color.rgb(73, 186, 35), Color.rgb(236, 247, 247)};

    int[] overflow_Color = {Color.RED, Color.rgb(236, 247, 247)};

    //-------------------------Create the pie graph--------------------------------

    private PieData generatePieData() {

        int count = 2;

        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();

        for(int i = 0; i < count; i++) {
            xVals.add(xvalues[i]);
            entries1.add(new Entry(consumption[i], i));
        }

        PieDataSet ds1 = new PieDataSet(entries1, "");
        if(is_overflow){
            ds1.setColors(overflow_Color);
        }else {
            ds1.setColors(chartColor);
        }
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.rgb(236, 247, 247));
        ds1.setValueTextSize(13f);

        PieData d = new PieData(xVals, ds1);
        d.setValueFormatter(new PercentFormatter());
        d.setValueTextSize(13f);

        return d;
    }

    //--------------------------Keyboard movements-------------------------------

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus){

            if (v.getId() == R.id.quota_layout && !hasFocus) {
                float shaded = user_info.getmonthcons()*100/Float.parseFloat(quota_val.getText().toString());

                if(shaded > 100) {
                    consumption[0] = (shaded%100);
                    is_overflow = true;
                    String ovf_str = String.format("Went beyond Quota by: %.2f", ((user_info.getmonthcons()*100/Float.parseFloat(quota_val.getText().toString())) - 100.00));
                    overflow.setText(ovf_str+"%");
                    overflow.setVisibility(View.VISIBLE);
                }else{
                    consumption[0] = shaded;
                    is_overflow = false;
                    overflow.setVisibility(View.INVISIBLE);
                }
                consumption[1] = 100 - consumption[0];

                mChart.setData(generatePieData());

                mChart.invalidate();
                mChart.animateY(1500);
            }
        }
    }

    //--------------------------Holds the main code-------------------------------

    @Override
    public void fragmentBecameVisible() {
        if(onCreateViewCalled) {
            title.setTranslationY(-150f);
            title.animate().translationY(0f).setDuration(750);
            mChart.invalidate();
            mChart.animateY(1500);
            cons.setVisibility(View.INVISIBLE);
            final Handler delay = new Handler();
            delay.postDelayed(new Runnable() {
                @Override
                public void run() {
                    final Animation fade_in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
                    fade_in.setDuration(1200);
                    cons.startAnimation(fade_in);
                    final Handler delay1 = new Handler();
                    delay1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cons.setVisibility(View.VISIBLE);
                        }
                    }, 1200);
                }
            }, 900);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home__ellie__monthly, container, false);
        overflow = (TextView)v.findViewById(R.id.overflow);
        float quota = 1000.00f, shaded;
        title = (TextView)v.findViewById(R.id.home_monthly_section_title);
        title.setTranslationY(-150f);
        title.animate().translationY(0f).setDuration(750);

        cons = (TextView)v.findViewById(R.id.curr_cons);

        cons.setText("₱" + user_info.getmonthcons());

        quota_val = (EditText)v.findViewById(R.id.quota_layout);

        View.OnFocusChangeListener ofc_quota = new MyFocusChangeListener();
        quota_val.setOnFocusChangeListener(ofc_quota);

        shaded = user_info.getmonthcons()*100/quota;

        if(shaded > 100) {
            consumption[0] = (shaded%100);
            is_overflow = true;
            String ovf_str = String.format("Went beyond Quota by: %.2f", ((user_info.getmonthcons()*100/quota) - 100.00));
            overflow.setText(ovf_str+"%");
            overflow.setVisibility(View.VISIBLE);
        }else{
            consumption[0] = shaded;
            is_overflow = false;
            overflow.setVisibility(View.INVISIBLE);
        }
        consumption[1] = 100 - consumption[0];


        mChart = (PieChart) v.findViewById(R.id.monthly_chart);
        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.getLegend().setEnabled(false);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleRadius(60f);
        mChart.setTransparentCircleRadius(70f);
        Calendar mcurrentTime = Calendar.getInstance();
        int curr_month = mcurrentTime.get(Calendar.MONTH);
        mChart.setCenterText(user_info.months[curr_month].toUpperCase() + "\n");
        mChart.setCenterTextSize(15f);
        mChart.setCenterTextColor(Color.parseColor("#00796B"));

        mChart.setData(generatePieData());
        mChart.invalidate();
        mChart.animateY(1500);
        cons.setVisibility(View.INVISIBLE);

        final Handler delay = new Handler();
        delay.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Animation fade_in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
                fade_in.setDuration(1200);
                cons.startAnimation(fade_in);
                final Handler delay1 = new Handler();
                delay1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cons.setVisibility(View.VISIBLE);
                    }
                }, 1200);
            }
        }, 900);

        onCreateViewCalled = true;
        return v;
    }
}
