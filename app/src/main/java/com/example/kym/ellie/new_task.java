package com.example.kym.ellie;

//--------------------------Holds the code for making/editing a task--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class new_task extends AppCompatActivity {

    GradientDrawable circle1;
    GradientDrawable circle2;
    GradientDrawable circle3;

    Menu appbar_menu;
    MenuItem save;
    MenuItem edit;

    ImageView bg1;
    ImageView bg2;
    ImageView bg3;

    ImageView dev_icon;
    ImageView command_icon;
    ImageView time_icon;

    EditText dev_name;
    EditText time;
    TextView command;

    TextView dev_name_error, time_error;

    TextInputLayout layout_dev_name;
    TextInputLayout layout_time;

    GradientDrawable bg_dev_name;
    GradientDrawable bg_time;

    String prev_name, prev_time, prev_status;
    boolean prev_command;

    Switch command_toggle;

    boolean is_new, is_editing = false, changed = false;

    int icon_id;

    private AlertDialog choicesDialog;

    //--------------------------Create the delete dialog--------------------------------

    private void deleting() {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);
        exit.setTitle("");
        exit.setMessage("Are you sure you want to delete this task?");
        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("deleted", true);
                        resultIntent.putExtra("changed", false);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }


    //--------------------------Create the exiting page dialog-------------------------------

    private void exiting(String message) {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);
        exit.setTitle("");
        exit.setMessage(message);

        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(is_new) {
                            finish();
                            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        }else{
                            edit.setVisible(true);
                            save.setVisible(false);
                            is_editing = false;
                            dev_name.setText(prev_name);
                            time.setText(prev_time);
                            command_toggle.setChecked(prev_command);
                            command.setText(prev_status);
                            update_icon(prev_name);
                            init_edit(2);
                        }
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }

    //--------------------------Update the icon whenever a new device is chosen--------------------------------

    private void update_icon(String dev_name){
        final Animation fadeout = AnimationUtils.loadAnimation(new_task.this, android.R.anim.fade_out);
        fadeout.setDuration(500);
        final Animation fadein = AnimationUtils.loadAnimation(new_task.this, android.R.anim.fade_in);
        fadein.setDuration(500);
        dev_icon.startAnimation(fadeout);
        dev_icon.setVisibility(View.INVISIBLE);
        final Handler delay1 = new Handler();
        int i;
        for(i = 0; i < Devices.Devices.size(); i++){
            if(dev_name.matches(Devices.Devices.get(i).Name)){
                break;
            }
        }

        final int icon = Devices.Devices.get(i).mIcon;
        icon_id = icon;

        delay1.postDelayed(new Runnable() {
            @Override
            public void run() {
                dev_icon.setImageDrawable(getResources().getDrawable(icon));
                dev_icon.startAnimation(fadein);
                dev_icon.setVisibility(View.VISIBLE);
            }
        }, 700);
    }

    //--------------------------Error checking for some fields--------------------------------

    private boolean error_check(){

        boolean error = false;

        Animation shake_ic = AnimationUtils.loadAnimation(this, R.anim.icon_shake_edit_device);
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        if(dev_name.getText().toString().matches("")){
            circle1.setColor(Color.RED);
            bg1.startAnimation(shake);
            dev_icon.startAnimation(shake_ic);
            bg_dev_name.setStroke(6, Color.RED);
            dev_name_error.setText("Device name is required");
            error = true;
        }

        if(time.getText().toString().matches("")){
            circle3.setColor(Color.RED);
            bg3.startAnimation(shake);
            time_icon.startAnimation(shake_ic);
            bg_time.setStroke(6, Color.RED);
            time_error.setText("Time is required");
            error = true;
        }

        return error;
    }

    private void setColorFilter(View v, Integer filter) {
        if (filter == null) v.getBackground().clearColorFilter();
        else {
            LightingColorFilter darken = new LightingColorFilter(filter, 0x000000);
            v.getBackground().setColorFilter(darken);
        }
        v.getBackground().invalidateSelf();
    }

    //--------------------------Create the choices dialog--------------------------------

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.toString().matches("Edit")) {
            setTitle("Editing Task");
            edit.setVisible(false);
            save.setVisible(true);
            is_editing = true;
            prev_name = dev_name.getText().toString();
            prev_time = time.getText().toString();
            prev_status = command.getText().toString();
            if(command_toggle.isChecked()){
                prev_command = true;
            }else{
                prev_command = false;
            }
            init_edit(1);
        }else if (item.toString().matches("Save")) {
            setTitle("Task");
            edit.setVisible(true);
            save.setVisible(false);
            is_editing = false;
            init_edit(2);
            changed = true;
        }else if(is_new|is_editing) {
            if(is_new){
                exiting("Are you sure you want to stop creating new task?");
            }else{
                exiting("Are you sure you want to cancel all changes made?");
            }
        }else{
            Intent resultIntent = new Intent();
            resultIntent.putExtra("deleted", false);
            resultIntent.putExtra("changed", changed);
            resultIntent.putExtra("dev_name", dev_name.getText().toString());
            resultIntent.putExtra("command", command.getText().toString());
            resultIntent.putExtra("time", time.getText().toString());
            resultIntent.putExtra("icon", icon_id);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
        return true;
    }

    private void open_choices() {
        AlertDialog.Builder choices = new AlertDialog.Builder(this);
        final String[] choices_array = new String[Devices.Devices.size()];
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.choices_layout, null);
        choices.setView(convertView);
        final TextView clicker = (TextView)convertView.findViewById(R.id.click2srch);
        final ListView choice_list = (ListView) convertView.findViewById(R.id.choice_list);
        final SearchView search_choice = (SearchView)convertView.findViewById(R.id.search_choice);

        final EditText search_hint = (EditText) search_choice.findViewById(R.id.search_src_text);
        search_hint.setHintTextColor(Color.parseColor("#DBDBDB"));
        search_hint.setTypeface(null, Typeface.ITALIC);

        choices.setTitle("Devices");
        search_choice.setQueryHint("Device Name");
        for(int i = 0; i < Devices.Devices.size(); i++){
            choices_array[i] = Devices.Devices.get(i).Name;
        }

        final ArrayAdapter<String> choice_adapter = new ArrayAdapter<String>(this, R.layout.choices_item ,choices_array);
        choice_list.setAdapter(choice_adapter);

        choices.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
                        dialog.dismiss();
                    }
                });

        choice_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
                dev_name.setText(choice_adapter.getItem(position));
                choicesDialog.dismiss();
                update_icon(dev_name.getText().toString());
            }
        });

        search_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_choice.setIconified(false);
                clicker.setVisibility(View.INVISIBLE);
            }
        });

        search_choice.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicker.setVisibility(View.INVISIBLE);
            }
        });

        search_choice.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                clicker.setVisibility(View.VISIBLE);
                return false;
            }
        });

        search_choice.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.matches("")) {
                    search_hint.setTypeface(null, Typeface.ITALIC);
                } else {
                    search_hint.setTypeface(null, Typeface.NORMAL);
                }
                choice_adapter.getFilter().filter(newText);
                return true;
            }
        });

        choicesDialog = choices.create() ;
        choicesDialog.show();
    }

    //--------------------------Create the buttons for the nav bar--------------------------------

    public boolean onCreatePanelMenu (int featureId, Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.new_task_menu, menu);
        edit = menu.findItem(R.id.action_edit);
        save = menu.findItem(R.id.action_save);
        save.setVisible(false);
        if(is_new){
            edit.setVisible(false);
        }
        appbar_menu = menu;
        return true;
    }

    //--------------------------Some initialization--------------------------------

    private void init_edit(int is_edit){
        if(is_edit == 1){
            bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
            bg_time.setStroke(2, Color.parseColor("#80CBC4"));

            layout_dev_name.setHint("Device name");
            layout_time.setHint("Time");

            command_toggle.setClickable(true);

            layout_dev_name.animate().translationY(0f).setDuration(700);
            layout_time.animate().translationY(0f).setDuration(700);
        }else if(is_edit == 0){
            bg_dev_name.setStroke(1, Color.WHITE);
            bg_time.setStroke(1, Color.WHITE);

            command_toggle.setClickable(false);

            layout_dev_name.setHint("");
            layout_time.setHint("");

            layout_dev_name.setTranslationY(-30f);
            layout_time.setTranslationY(-30f);
        }else{
            bg_dev_name.setStroke(1, Color.WHITE);
            bg_time.setStroke(1, Color.WHITE);

            layout_dev_name.setHint("");
            layout_time.setHint("");

            command_toggle.setClickable(false);

            layout_dev_name.animate().translationY(-30f).setDuration(700);
            layout_time.animate().translationY(-30f).setDuration(700);
        }
    }

    //--------------------------Tell Rpi that a task is created--------------------------------

    private void create_task(){
        Intent resultIntent = new Intent();
        resultIntent.putExtra("dev_name", dev_name.getText().toString());
        resultIntent.putExtra("command", command.getText().toString());
        resultIntent.putExtra("time", time.getText().toString());
        resultIntent.putExtra("icon", icon_id);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
    }

    //--------------------------holds the main code--------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bg1 = (ImageView)findViewById(R.id.bg_circle);
        bg2 = (ImageView)findViewById(R.id.bg_circle1);
        bg3 = (ImageView)findViewById(R.id.bg_circle2);

        layout_dev_name = (TextInputLayout)findViewById(R.id.dev_name_layout);
        layout_time = (TextInputLayout)findViewById(R.id.time_value_layout);

        LayerDrawable bg = (LayerDrawable)bg1.getBackground();
        circle1 = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);
        bg = (LayerDrawable)bg2.getBackground();
        circle2 = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);
        bg = (LayerDrawable)bg3.getBackground();
        circle3 = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);

        circle1.setColor(getResources().getColor(R.color.teal));
        circle2.setColor(getResources().getColor(R.color.teal));
        circle3.setColor(getResources().getColor(R.color.teal));

        dev_name = (EditText)findViewById(R.id.dev_name);
        time = (EditText)findViewById(R.id.time);

        bg = (LayerDrawable)dev_name.getBackground();
        bg_dev_name = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);
        bg = (LayerDrawable)time.getBackground();
        bg_time = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);

        dev_icon = (ImageView)findViewById(R.id.dev_icon);
        command_icon = (ImageView)findViewById(R.id.command_icon);
        time_icon = (ImageView)findViewById(R.id.time_icon);
        command = (TextView)findViewById(R.id.command);
        time_error = (TextView)findViewById(R.id.time_error);
        dev_name_error = (TextView)findViewById(R.id.dev_name_error);

        command_toggle = (Switch)findViewById(R.id.command_toggle);

        bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
        bg_time.setStroke(2, Color.parseColor("#80CBC4"));

        Button task_button = (Button)findViewById(R.id.task_button);
        bg = (LayerDrawable)task_button.getBackground();
        GradientDrawable bg_button = (GradientDrawable)bg.findDrawableByLayerId(R.id.btn_bg);

        if(getIntent().getExtras().containsKey("device_name")) {
            setTitle("Task");
            is_new = false;
            dev_name.setText(getIntent().getExtras().getString("device_name"));
            time.setText(getIntent().getExtras().getString("time"));
            if(getIntent().getExtras().getBoolean("command")){
                command_toggle.setChecked(true);
                command.setText("ON");
            }else{
                command_toggle.setChecked(false);
                command.setText("OFF");
            }
            dev_icon.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("icon")));
            icon_id = getIntent().getExtras().getInt("icon");
            task_button.setText("Delete Task");
            bg_button.setColor(Color.RED);
            init_edit(0);
        }else{
            setTitle("New Task");
            is_new = true;
            task_button.setText("Create Task");
            bg_button.setColor(getResources().getColor(R.color.teal));
        }

        command_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    command.setText("ON");
                } else {
                    command.setText("OFF");
                }
            }
        });

        dev_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_editing | is_new) {
                    circle1.setColor(getResources().getColor(R.color.teal));
                    dev_name_error.setText("");
                    bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
                    open_choices();
                }
            }
        });

        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (is_editing | is_new) {
                    circle3.setColor(getResources().getColor(R.color.teal));
                    time_error.setText("");
                    bg_time.setStroke(2, Color.parseColor("#80CBC4"));
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;

                    mTimePicker = new TimePickerDialog(new_task.this, new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String AM_PM, hour, minute;

                            if (selectedHour == 0 && selectedMinute == 0) {
                                AM_PM = "MN";
                            } else if (selectedHour == 12 && selectedMinute == 0) {
                                AM_PM = "NN";
                            } else if (selectedHour == 12) {
                                AM_PM = "PM";
                            } else if (selectedHour < 12) {
                                AM_PM = "AM";
                            } else {
                                AM_PM = "PM";
                                selectedHour -= 12;
                            }

                            if (selectedHour < 10) {
                                hour = "0" + selectedHour;
                            } else {
                                hour = "" + selectedHour;
                            }

                            if (selectedMinute < 10) {
                                minute = "0" + selectedMinute;
                            } else {
                                minute = "" + selectedMinute;
                            }
                            time.setText(hour + ":" + minute + " " + AM_PM);
                            //time.setText(hour + ":" + minute + ":00 " + AM_PM);
                        }
                    }, hour, minute, false);
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            }
        });

        task_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setColorFilter(v, 0xBDBDBD);
                        if (is_new) {
                            if(!error_check()){
                                create_task();
                            }
                        } else {
                            deleting();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Rect r = new Rect();
                        v.getLocalVisibleRect(r);
                        if (!r.contains((int) event.getX(), (int) event.getY())) {
                            setColorFilter(v, null);
                        }
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        setColorFilter(v, null);
                        break;
                }
                return false;
            }
        });

    }
}
