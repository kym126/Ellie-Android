package com.example.kym.ellie;

//--------------------------Holds the code for the Edit Devices PAge--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class edit_device extends AppCompatActivity {

    boolean is_new = false, is_editing = false, has_changed = false, is_on = false;
    String real_desc, real_name, real_app_type, real_brand, real_loc, changed="changed";
    int dev_id, list_pos, dev_code;
    NetworkHelper networkHelper = new NetworkHelper();

    String[] locationArray =  {
            "Basement",
            "Bedroom",
            "Comfort Room",
            "Dining Area",
            "Garage",
            "Garden",
            "Kitchen",
            "Laundry Room",
            "Living Room",
            "Patio",
            "Poolside",
            "Stock Room",
            "Terrace"
    };

    Button dev_btn;

    Animation shake;

    Menu appbar_menu;
    MenuItem save;
    MenuItem edit;
    MenuItem toggle;

    EditText dev_name;
    EditText dev_desc;
    EditText dev_app_type;
    EditText dev_brand;
    EditText dev_loc;

    GradientDrawable circle_icon;
    GradientDrawable bg_dev_name;
    GradientDrawable bg_dev_desc;
    GradientDrawable bg_dev_app_type;
    GradientDrawable bg_dev_brand;
    GradientDrawable bg_dev_loc;
    GradientDrawable bg_dev_btn;

    TextView error_name;
    TextView error_desc;
    TextView error_app_type;
    TextView error_brand;
    TextView error_loc;
    TextView placeholder;

    ImageView bg_circle;
    ImageView iconview;
    ImageView desc_icon;
    ImageView app_type_icon;
    ImageView brand_icon;
    ImageView loc_icon;

    TextInputLayout layout_dev_name;
    TextInputLayout layout_dev_desc;
    TextInputLayout layout_dev_app_type;
    TextInputLayout layout_dev_brand;
    TextInputLayout layout_dev_loc;

    //--------------------------Returns if a name has already been used--------------------------------

    private boolean check_name(String new_name){
        for(int i = 0; i < Devices.Devices.size(); i++){
            if(new_name.matches(Devices.Devices.get(i).Name)&&i != list_pos){
                return false;
            }
        }
        return  true;
    }

    //--------------------------Creates the dialog for saved devices--------------------------------

    private void saved_dialog(String message) {
        AlertDialog.Builder save = new AlertDialog.Builder(this);
        save.setTitle("");
        save.setMessage(message);
        final AlertDialog saveDialog = save.create();
        saveDialog.show();
        final Handler delay = new Handler();
        delay.postDelayed(new Runnable() {
            @Override
            public void run() {
                saveDialog.dismiss();
            }
        }, 1000);
    }

    //--------------------------Updates the icon whenever a new application type is clicked--------------------------------

    private void update_icon(){
        final Animation fadeout = AnimationUtils.loadAnimation(edit_device.this, android.R.anim.fade_out);
        fadeout.setDuration(500);
        final Animation fadein = AnimationUtils.loadAnimation(edit_device.this, android.R.anim.fade_in);
        fadein.setDuration(500);
        iconview.startAnimation(fadeout);
        iconview.setVisibility(View.INVISIBLE);
        final Handler delay1 = new Handler();
        delay1.postDelayed(new Runnable() {
            @Override
            public void run() {
                iconview.setImageDrawable(getResources().getDrawable(application_types.get_icon(dev_app_type.getText().toString())));
                iconview.startAnimation(fadein);
                iconview.setVisibility(View.VISIBLE);
            }
        }, 700);
    }

    //--------------------------Create generic loader--------------------------------


    private ProgressDialog create_loader(int save) {

        hideSoftKeyboard(edit_device.this, this.getCurrentFocus());
        ProgressDialog dialog = new ProgressDialog(edit_device.this);
        if(save == 0) {
            dialog.setMessage("Creating device");
        }else if(save == 1){
            dialog.setMessage("Saving device");
        }else if(save == 2){
            dialog.setMessage("Deleting device");
        }else{
            dialog.setMessage("Sending command");
        }
        return dialog;
    }

    private ProgressDialog custom_loader(String msg) {

        ProgressDialog dialog = new ProgressDialog(edit_device.this);
        dialog.setMessage(msg);
        return dialog;
    }

    //--------------------------Create complete message dialog--------------------------------


    private void show_complete() {
        hideSoftKeyboard(edit_device.this, this.getCurrentFocus());
        AlertDialog.Builder signup_complete = new AlertDialog.Builder(this);
        signup_complete.setTitle("Device Created");
        String ellie_message = "New device \"" + dev_name.getText().toString() + "\" created";
        signup_complete.setMessage(ellie_message);
        signup_complete.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(changed, true);
                        resultIntent.putExtra("device_id", dev_id);
                        resultIntent.putExtra("device_code", dev_code);
                        resultIntent.putExtra("device_name", dev_name.getText().toString());
                        resultIntent.putExtra("device_desc", dev_desc.getText().toString());
                        resultIntent.putExtra("device_app_type", dev_app_type.getText().toString());
                        resultIntent.putExtra("device_brand", dev_brand.getText().toString());
                        resultIntent.putExtra("device_loc", dev_loc.getText().toString());
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                    }
                });

        AlertDialog helpDialog = signup_complete.create();
        helpDialog.show();
    }

    //--------------------------Create error dialog--------------------------------

    private void show_error(int state) {

        AlertDialog.Builder create_error = new AlertDialog.Builder(this);
        create_error.setTitle("Error");
        if(state == 0){
            create_error.setMessage("There was an error creating device \""+dev_name.getText().toString()+"\". Please make sure that you haven't registered this plug before.");
        }else if(state == 1){
            create_error.setMessage("There was an error updating \""+dev_name.getText().toString()+"\". Please make sure that everything is correct before you try again.");
        }else{
            create_error.setMessage("There was an error deleting \""+dev_name.getText().toString()+"\". Please make sure that that you are connected to the correct Rpi.");
        }
        create_error.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog helpDialog = create_error.create();
        helpDialog.show();
    }

    //--------------------------Creates and functionalities of the buttons on the nav bar--------------------------------

    public boolean onCreatePanelMenu (int featureId, Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.edit_device_menu, menu);
        edit = menu.findItem(R.id.action_edit);
        save = menu.findItem(R.id.action_save);
        toggle = menu.findItem(R.id.action_toggle);
        save.setVisible(false);
        if(is_new){
            edit.setVisible(false);
            toggle.setVisible(false);
        }else {
            if (is_on) {
                toggle.setIcon(getResources().getDrawable(R.drawable.command_icon_off));
                is_on = true;
            } else {
                toggle.setIcon(getResources().getDrawable(R.drawable.command_icon));
                is_on = false;
            }
        }
        appbar_menu = menu;
        return true;
    }

    void save(boolean save){
        if(save == true){
            real_name = dev_name.getText().toString();
            real_desc = dev_desc.getText().toString();
            real_app_type = dev_app_type.getText().toString();
            real_brand = dev_brand.getText().toString();
            real_loc = dev_loc.getText().toString();
        }else{
            dev_name.setText(real_name);
            dev_desc.setText(real_desc);
            dev_app_type.setText(real_app_type);
            dev_brand.setText(real_brand);
            dev_loc.setText(real_loc);
            update_icon();
        }
    }

    private void switch_value(final boolean switch_val){
        final ProgressDialog loader;
        JSONObject command = new JSONObject();
        if (switch_val == false) {
            loader = custom_loader("Turning \""+dev_name.getText().toString()+"\" OFF.");
        }else{
            loader = custom_loader("Turning \""+dev_name.getText().toString()+"\" ON.");
        }
        try {
            if (switch_val == false) {
                command.put("command", "OFF");
            }else{
                command.put("command", "ON");
            }
            networkHelper.post("api/v1/devices/" + dev_id + "/sendCommand", command.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            AlertDialog.Builder save_device = new AlertDialog.Builder(edit_device.this);
                            save_device.setTitle("Network Error");
                            save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                            save_device.setNeutralButton("CANCEL",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            save_device.setPositiveButton("RETRY",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            switch_value(switch_val);
                                        }
                                    });
                            AlertDialog helpDialog = save_device.create();
                            helpDialog.show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            if (switch_val == false) {
                                saved_dialog("Turned \"" + dev_name.getText().toString() + "\" OFF");
                            } else {
                                saved_dialog("Turned \"" + dev_name.getText().toString() + "\" ON");
                            }
                        }
                    });
                }
            });
        }catch (Exception e){

        }
    }

    //--------------------------Sends info to the RPI tat device is updated--------------------------------

    private void upd_dev(final String json){
        final ProgressDialog loader = create_loader(1);
        loader.show();
        String put_req = "api/v1/devices/"+dev_id;
        networkHelper.put(put_req, json, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_device.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        upd_dev(json);
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.message().matches("OK")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            setTitle("Device");
                            edit.setVisible(true);
                            save.setVisible(false);
                            is_editing = false;
                            make_focusable(false);
                            set_dec_none(false);
                            has_changed = true;
                            saved_dialog("Updated device \"" + dev_name.getText().toString() + "\"");
                        }
                    });
                } else {
                }

            }
        });
    }

    //--------------------------Functioanlities for other buttons on nav bar--------------------------------

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.toString().matches("toggle")) {
            if(is_on) {
                toggle.setIcon(getResources().getDrawable(R.drawable.command_icon));
                is_on = false;
            }else{
                toggle.setIcon(getResources().getDrawable(R.drawable.command_icon_off));
                is_on = true;
            }
            has_changed = true;
            switch_value(is_on);
        }else if(item.toString().matches("Edit")) {
            setTitle("Editing Device");
            edit.setVisible(false);
            save.setVisible(true);
            is_editing = true;
            make_focusable(true);
            set_dec_none(true);
            save(true);
        }else if (item.toString().matches("Save")) {
            hideSoftKeyboard(edit_device.this, this.getCurrentFocus());
            clear_focus();
            if(!error_check()) {
                JSONObject new_dev = new JSONObject();
                try {
                    new_dev.put("name", dev_name.getText().toString());
                    new_dev.put("description", dev_desc.getText().toString());
                    new_dev.put("applianceType", dev_app_type.getText().toString());
                    new_dev.put("brandName", dev_brand.getText().toString());
                    new_dev.put("location", dev_loc.getText().toString());
                }catch(JSONException e){
                }
                upd_dev(new_dev.toString());
            }
        }else if(is_new|is_editing) {
            exiting();
        }else{
            Intent resultIntent = new Intent();
            resultIntent.putExtra("active", is_on);
            resultIntent.putExtra(changed, has_changed);
            resultIntent.putExtra("device_id", dev_id);
            resultIntent.putExtra("device_code", dev_code);
            resultIntent.putExtra("device_name", dev_name.getText().toString());
            resultIntent.putExtra("device_desc", dev_desc.getText().toString());
            resultIntent.putExtra("device_app_type", dev_app_type.getText().toString());
            resultIntent.putExtra("device_brand", dev_brand.getText().toString());
            resultIntent.putExtra("device_loc", dev_loc.getText().toString());
            resultIntent.putExtra("deleted", false);
            resultIntent.putExtra("list_pos", list_pos);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
        return true;
    }

    //--------------------------Creates dialog for exiting the page--------------------------------

    private void exiting() {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);
        exit.setTitle("");
        if(is_new){
            exit.setMessage("Are you sure you want to stop creating new device?");
        }else{
            exit.setMessage("Are you sure you want to cancel all changes made?");
        }

        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(is_new) {
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra(changed, false);
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        }else{
                            is_editing = false;
                            save.setVisible(false);
                            edit.setVisible(true);
                            make_focusable(false);
                            save(false);
                            set_dec_none(false);
                            setTitle("Device");
                            revert_states();
                        }
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }

    //--------------------------Creates dialog wfor deleting the device--------------------------------

    private void deleting() {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);
        exit.setTitle("");

        if(is_editing){
            exit.setMessage("Are you sure you want to delete the device \"" + real_name + "\"?");
        }else{
            exit.setMessage("Are you sure you want to delete the device \"" + dev_name.getText().toString() + "\"?");
        }
        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        delete_dev();
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }

    private AlertDialog choicesDialog;

    //--------------------------Creates the dialog for choices--------------------------------

    private void open_choices(final boolean is_app_type) {
        AlertDialog.Builder choices = new AlertDialog.Builder(this);
        final String[] choices_array;
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.choices_layout, null);
        choices.setView(convertView);
        final TextView clicker = (TextView)convertView.findViewById(R.id.click2srch);
        final ListView choice_list = (ListView) convertView.findViewById(R.id.choice_list);
        final SearchView search_choice = (SearchView)convertView.findViewById(R.id.search_choice);

        final EditText search_hint = (EditText) search_choice.findViewById(R.id.search_src_text);
        search_hint.setHintTextColor(Color.parseColor("#DBDBDB"));
        search_hint.setTypeface(null, Typeface.ITALIC);

        if (is_app_type){
            choices.setTitle("Appliance Type");
            search_choice.setQueryHint("Appliance Type");
            choices_array = application_types.app_typeArray;
        }else{
            choices.setTitle("Location");
            search_choice.setQueryHint("Location");
            choices_array = locationArray;
        }

        final ArrayAdapter<String> choice_adapter = new ArrayAdapter<String>(this, R.layout.choices_item ,choices_array);
        choice_list.setAdapter(choice_adapter);

        choices.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (is_app_type) {
                            bg_dev_app_type.setStroke(1, Color.parseColor("#80CBC4"));
                        } else {
                            bg_dev_loc.setStroke(1, Color.parseColor("#80CBC4"));
                        }
                        dialog.dismiss();
                    }
                });

        choice_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (is_app_type) {
                    dev_app_type.setText(choice_adapter.getItem(position));
                    bg_dev_app_type.setStroke(1, Color.parseColor("#80CBC4"));
                } else {
                    dev_loc.setText(choice_adapter.getItem(position));
                    bg_dev_loc.setStroke(1, Color.parseColor("#80CBC4"));
                }
                hideSoftKeyboard(edit_device.this, view);
                choicesDialog.dismiss();
                clear_focus();
                if(is_app_type){
                    update_icon();
                }
            }
        });

        search_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_choice.setIconified(false);
                clicker.setVisibility(View.INVISIBLE);
            }
        });

        search_choice.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicker.setVisibility(View.INVISIBLE);
            }
        });

        search_choice.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                clicker.setVisibility(View.VISIBLE);
                return false;
            }
        });

        search_choice.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.matches("")) {
                    search_hint.setTypeface(null, Typeface.ITALIC);
                } else {
                    search_hint.setTypeface(null, Typeface.NORMAL);
                }
                choice_adapter.getFilter().filter(newText);
                return true;
            }
        });

        choicesDialog = choices.create() ;
        choicesDialog.show();
    }

    //--------------------------Holds the animations for edit and update--------------------------------

    public void make_focusable(boolean focus){
        if(focus == true){

            layout_dev_desc.animate().translationY(0f).setDuration(700);
            layout_dev_app_type.animate().translationY(0f).setDuration(700);
            layout_dev_brand.animate().translationY(0f).setDuration(700);
            layout_dev_loc.animate().translationY(0f).setDuration(700);

            final Handler delay = new Handler();

            delay.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dev_name.setFocusableInTouchMode(true);
                    dev_name.setFocusable(true);
                    dev_desc.setFocusableInTouchMode(true);
                    dev_desc.setFocusable(true);
                    dev_brand.setFocusableInTouchMode(true);
                    dev_brand.setFocusable(true);

                    bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
                    bg_dev_desc.setStroke(1, Color.parseColor("#80CBC4"));
                    bg_dev_app_type.setStroke(1, Color.parseColor("#80CBC4"));
                    bg_dev_brand.setStroke(1, Color.parseColor("#80CBC4"));
                    bg_dev_loc.setStroke(1, Color.parseColor("#80CBC4"));

                    layout_dev_name.setHint("Device name");
                    layout_dev_desc.setHint("Description");
                    layout_dev_app_type.setHint("Appliance type");
                    layout_dev_brand.setHint("Brand name");
                    layout_dev_loc.setHint("Location");

                }
            }, 700);

        }else{
            dev_name.setFocusable(false);
            dev_desc.setFocusable(false);
            dev_brand.setFocusable(false);

            bg_dev_name.setStroke(6, Color.parseColor("#00796B"));
            bg_dev_desc.setStroke(1, Color.WHITE);
            bg_dev_app_type.setStroke(1, Color.WHITE);
            bg_dev_brand.setStroke(1, Color.WHITE);
            bg_dev_loc.setStroke(1, Color.WHITE);

            layout_dev_name.setHint("");
            layout_dev_desc.setHint("");
            layout_dev_app_type.setHint("");
            layout_dev_brand.setHint("");
            layout_dev_loc.setHint("");

            layout_dev_desc.animate().translationY(-15f).setDuration(700);
            layout_dev_app_type.animate().translationY(-20f).setDuration(700);
            layout_dev_brand.animate().translationY(-20f).setDuration(700);
            layout_dev_loc.animate().translationY(-20f).setDuration(700);

        }
    }

    //--------------------------Holds code for initialization--------------------------------

    void init(boolean is_new){
        if(is_new){
            dev_name.setFocusableInTouchMode(true);
            dev_name.setFocusable(true);
            dev_desc.setFocusableInTouchMode(true);
            dev_desc.setFocusable(true);
            dev_brand.setFocusableInTouchMode(true);
            dev_brand.setFocusable(true);

            bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
            bg_dev_desc.setStroke(1, Color.parseColor("#80CBC4"));
            bg_dev_app_type.setStroke(1, Color.parseColor("#80CBC4"));
            bg_dev_brand.setStroke(1, Color.parseColor("#80CBC4"));
            bg_dev_loc.setStroke(1, Color.parseColor("#80CBC4"));

            layout_dev_name.setHint("Device name");
            layout_dev_desc.setHint("Description");
            layout_dev_app_type.setHint("Appliance type");
            layout_dev_brand.setHint("Brand name");
            layout_dev_loc.setHint("Location");
        }else {
            dev_name.setFocusable(false);
            dev_desc.setFocusable(false);
            dev_brand.setFocusable(false);

            bg_dev_name.setStroke(6, Color.parseColor("#00796B"));
            bg_dev_desc.setStroke(1, Color.WHITE);
            bg_dev_app_type.setStroke(1, Color.WHITE);
            bg_dev_brand.setStroke(1, Color.WHITE);
            bg_dev_loc.setStroke(1, Color.WHITE);

            layout_dev_name.setHint("");
            layout_dev_desc.setHint("");
            layout_dev_app_type.setHint("");
            layout_dev_brand.setHint("");
            layout_dev_loc.setHint("");

            layout_dev_desc.setTranslationY(-15f);
            layout_dev_app_type.setTranslationY(-20f);
            layout_dev_brand.setTranslationY(-20f);
            layout_dev_loc.setTranslationY(-20f);

            dev_name.setTranslationX(-730f);
            dev_name.animate().translationX(0).setDuration(1500);
            iconview.animate().rotation(720f).setDuration(1500);
        }
    }

    //--------------------------Keyboard and animation event handlers--------------------------------

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus){

            if (v.getId() == R.id.dev_name && hasFocus){
                error_name.setText("");
                bg_dev_name.setStroke(6, Color.parseColor("#26A69A"));
                circle_icon.setColor(Color.parseColor("#00796B"));
            }else if(v.getId() == R.id.dev_desc && hasFocus){
                bg_dev_desc.setStroke(2, Color.parseColor("#26A69A"));
            }else if(v.getId() == R.id.dev_brand && hasFocus){
                error_brand.setText("");
                bg_dev_brand.setStroke(2, Color.parseColor("#26A69A"));
                brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.brand_icon));
            }

            if (v.getId() == R.id.dev_name && !hasFocus){
                bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.dev_desc && !hasFocus){
                bg_dev_desc.setStroke(1, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.dev_brand && !hasFocus){
                bg_dev_brand.setStroke(1, Color.parseColor("#80CBC4"));
            }

            if ((v.getId() == R.id.dev_name && !hasFocus)||(v.getId() == R.id.dev_desc && !hasFocus)||(v.getId() == R.id.dev_brand && !hasFocus)) {
                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }else{
                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        }
    }

    void set_dec_none(boolean edit){
        if(dev_desc.getText().toString().matches("")&&edit == true) {
            placeholder.animate().translationY(0f).setDuration(700);
            final Handler delay = new Handler();
            delay.postDelayed(new Runnable() {
                @Override
                public void run() {
                    placeholder.setVisibility(View.INVISIBLE);
                }
            }, 700);
        }else if(dev_desc.getText().toString().matches("")&&edit == false){
            placeholder.setVisibility(View.VISIBLE);
            placeholder.animate().translationY(-14f).setDuration(700);
        }
    }

    private void setColorFilter(View v, Integer filter) {
        if (filter == null) v.getBackground().clearColorFilter();
        else {
            LightingColorFilter darken = new LightingColorFilter(filter, 0x000000);
            v.getBackground().setColorFilter(darken);
        }
        v.getBackground().invalidateSelf();
    }

    private void focus_clear(){
        bg_dev_name.setStroke(2, Color.parseColor("#80CBC4"));
        bg_dev_desc.setStroke(1, Color.parseColor("#80CBC4"));
        bg_dev_brand.setStroke(1, Color.parseColor("#80CBC4"));
        dev_name.clearFocus();
        dev_desc.clearFocus();
        dev_brand.clearFocus();
    }

    private void revert_states(){
        error_name.setText("");
        circle_icon.setColor(Color.parseColor("#00796B"));
        error_brand.setText("");
        brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.brand_icon));
    }

    //--------------------------Error checking for different fields--------------------------------

    private boolean error_check(){
        boolean error = false;

        if(dev_name.getText().toString().matches("")){
            Animation shake_ic = AnimationUtils.loadAnimation(this, R.anim.icon_shake_edit_device);
            circle_icon.setColor(Color.RED);
            bg_circle.startAnimation(shake);
            iconview.startAnimation(shake_ic);
            bg_dev_name.setStroke(6, Color.RED);
            error_name.setText("Device name is required");
            error = true;
        }

        if(!check_name(dev_name.getText().toString())){
            Animation shake_ic = AnimationUtils.loadAnimation(this, R.anim.icon_shake_edit_device);
            circle_icon.setColor(Color.RED);
            bg_circle.startAnimation(shake);
            iconview.startAnimation(shake_ic);
            bg_dev_name.setStroke(6, Color.RED);
            error_name.setText("This name is already used");
            error = true;
        }

        if(dev_brand.getText().toString().matches("")){
            brand_icon.setImageDrawable(getResources().getDrawable(R.drawable.brand_icon_error));
            brand_icon.startAnimation(shake);
            bg_dev_brand.setStroke(2, Color.RED);
            error_brand.setText("Brand name is required");
            error = true;
        }

        if(is_new){
            if(dev_app_type.getText().toString().matches("")){
                app_type_icon.setImageDrawable(getResources().getDrawable(R.drawable.appliance_icon_error));
                app_type_icon.startAnimation(shake);
                bg_dev_app_type.setStroke(2, Color.RED);
                error_app_type.setText("Application type is required");
                error = true;
            }

            if(dev_loc.getText().toString().matches("")){
                loc_icon.setImageDrawable(getResources().getDrawable(R.drawable.location_icon_error));
                loc_icon.startAnimation(shake);
                bg_dev_loc.setStroke(2, Color.RED);
                error_loc.setText("Location is required");
                error = true;
            }
        }

        return error;
    }

    //--------------------------Holds the code for the Devices Fragment(Page)--------------------------------

    public static void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    private void clear_focus(){
        dev_name.clearFocus();
        dev_app_type.clearFocus();
        dev_desc.clearFocus();
        dev_brand.clearFocus();
        dev_loc.clearFocus();
    }

    //--------------------------Tells Rpi that new device is made--------------------------------

    private void save_new_dev(){
        if(!error_check()){
            final ProgressDialog loader = create_loader(0);
            JSONObject new_dev = new JSONObject();
            try {
                new_dev.put("name", dev_name.getText().toString());
                new_dev.put("tedsId", dev_code);
                new_dev.put("description", dev_desc.getText().toString());
                new_dev.put("applianceType", dev_app_type.getText().toString());
                new_dev.put("brandName", dev_brand.getText().toString());
                new_dev.put("UserId", user_info.getid());
                new_dev.put("location", dev_loc.getText().toString());
            }catch(JSONException e){
            }
            loader.show();
            networkHelper.post("api/v1/devices", new_dev.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            AlertDialog.Builder save_device = new AlertDialog.Builder(edit_device.this);
                            save_device.setTitle("Network Error");
                            save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                            save_device.setNeutralButton("CANCEL",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            save_device.setPositiveButton("RETRY",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            save_new_dev();
                                        }
                                    });
                            AlertDialog helpDialog = save_device.create();
                            helpDialog.show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.message().matches("OK")) {
                        try {
                            JSONObject result = new JSONObject(response.body().string());
                            JSONObject device = new JSONObject();
                            if (result.has("results")) {
                                device = result.optJSONObject("results");
                            }
                            if (device.has("id")) {
                                dev_id = device.optInt("id");
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loader.dismiss();
                                    show_complete();
                                }
                            });
                        } catch (Exception e) {
                        }

                    } else {
                    }

                }
            });
        }
    }

    //--------------------------Tells Rpi that a device was deleted--------------------------------

    private void delete_dev(){
        final ProgressDialog loader = create_loader(2);
        String delete_req = "api/v1/devices/"+dev_id;
        loader.show();
        networkHelper.delete(delete_req, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_device.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        delete_dev();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.toString();
                Log.d("response", response_str);
                if (response.message().matches("OK")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra(changed, false);
                            resultIntent.putExtra("deleted", true);
                            resultIntent.putExtra("list_pos", list_pos);
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            show_error(2);
                        }
                    });
                }

            }
        });
    }

    //--------------------------Contains the main function--------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_device);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        dev_id = getIntent().getExtras().getInt("device_id");
        dev_code = getIntent().getExtras().getInt("device_code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bg_circle = (ImageView)findViewById(R.id.bg_circle);
        LayerDrawable circle = (LayerDrawable)bg_circle.getBackground();
        circle_icon = (GradientDrawable)circle.findDrawableByLayerId(R.id.circle_bg);
        circle_icon.setColor(Color.parseColor("#00796B"));

        dev_name = (EditText)findViewById(R.id.dev_name);
        dev_desc = (EditText)findViewById(R.id.dev_desc);
        dev_app_type = (EditText)findViewById(R.id.dev_app_type);
        dev_brand = (EditText)findViewById(R.id.dev_brand);
        dev_loc = (EditText)findViewById(R.id.dev_loc);

        dev_btn = (Button)findViewById(R.id.device_btn);
        shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        layout_dev_name = (TextInputLayout)findViewById(R.id.dev_name_layout);
        layout_dev_desc = (TextInputLayout)findViewById(R.id.dev_desc_layout);
        layout_dev_app_type = (TextInputLayout)findViewById(R.id.dev_app_type_layout);
        layout_dev_brand = (TextInputLayout)findViewById(R.id.dev_brand_layout);
        layout_dev_loc = (TextInputLayout)findViewById(R.id.dev_loc_layout);


        iconview = (ImageView)findViewById(R.id.dev_icon);
        desc_icon = (ImageView)findViewById(R.id.desc_ic);
        app_type_icon = (ImageView)findViewById(R.id.app_type_ic);
        brand_icon = (ImageView)findViewById(R.id.brand_ic);
        loc_icon = (ImageView) findViewById(R.id.loc_ic);

        LayerDrawable bg;
        bg = (LayerDrawable)dev_name.getBackground();
        bg_dev_name = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);
        bg = (LayerDrawable)dev_desc.getBackground();
        bg_dev_desc = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)dev_app_type.getBackground();
        bg_dev_app_type = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)dev_brand.getBackground();
        bg_dev_brand = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)dev_loc.getBackground();
        bg_dev_loc = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)dev_btn.getBackground();
        bg_dev_btn = (GradientDrawable)bg.findDrawableByLayerId(R.id.btn_bg);

        error_name = (TextView)findViewById(R.id.dev_name_error);
        error_desc = (TextView)findViewById(R.id.dev_desc_error);
        error_app_type = (TextView)findViewById(R.id.dev_app_type_error);
        error_brand = (TextView)findViewById(R.id.dev_brand_error);
        error_loc = (TextView)findViewById(R.id.dev_loc_error);
        placeholder = (TextView)findViewById(R.id.placeholder);

        View.OnFocusChangeListener ofcListener_dev_name = new MyFocusChangeListener();
        dev_name.setOnFocusChangeListener(ofcListener_dev_name);

        View.OnFocusChangeListener ofcListener_dev_desc = new MyFocusChangeListener();
        dev_desc.setOnFocusChangeListener(ofcListener_dev_desc);

        View.OnFocusChangeListener ofcListener_dev_app_type = new MyFocusChangeListener();
        dev_app_type.setOnFocusChangeListener(ofcListener_dev_app_type);

        View.OnFocusChangeListener ofcListener_dev_brand = new MyFocusChangeListener();
        dev_brand.setOnFocusChangeListener(ofcListener_dev_brand);

        View.OnFocusChangeListener ofcListener_dev_loc = new MyFocusChangeListener();
        dev_loc.setOnFocusChangeListener(ofcListener_dev_loc);

        if(getIntent().getExtras().containsKey("device_name")){
            setTitle("Device");
            init(false);
            is_on = getIntent().getExtras().getBoolean("active");
            dev_name.setText(getIntent().getExtras().getString("device_name"));
            dev_desc.setText(getIntent().getExtras().getString("device_desc"));
            dev_app_type.setText(getIntent().getExtras().getString("device_app_type"));
            dev_brand.setText(getIntent().getExtras().getString("device_brand"));
            dev_loc.setText(getIntent().getExtras().getString("device_loc"));
            iconview.setImageDrawable(getResources().getDrawable(application_types.get_icon(dev_app_type.getText().toString())));
            list_pos = getIntent().getExtras().getInt("list_pos");
            real_desc = getIntent().getExtras().getString("device_desc");
            dev_btn.setText("Delete Device");
            bg_dev_btn.setColor(Color.RED);
            if(getIntent().getExtras().getString("device_desc").matches("")){
                placeholder.setVisibility(View.VISIBLE);
                placeholder.setTranslationY(-14f);
            }else{
                placeholder.setVisibility(View.INVISIBLE);
            }
        }else{
            init(true);
            is_new = true;
            setTitle("New Device");
            dev_btn.setText("Create Device");
            bg_dev_btn.setColor(Color.parseColor("#00897B"));
            list_pos = getIntent().getExtras().getInt("list_pos");
        }

        dev_app_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(edit_device.this, v);
                if(is_editing|is_new) {
                    focus_clear();
                    error_app_type.setText("");
                    bg_dev_app_type.setStroke(2, Color.parseColor("#26A69A"));
                    app_type_icon.setImageDrawable(getResources().getDrawable(R.drawable.appliance_icon));
                    open_choices(true);
                }
            }
        });

        dev_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(edit_device.this, v);
                if(is_editing|is_new) {
                    focus_clear();
                    error_loc.setText("");
                    bg_dev_loc.setStroke(2, Color.parseColor("#26A69A"));
                    loc_icon.setImageDrawable(getResources().getDrawable(R.drawable.location_icon));
                    open_choices(false);
                }
            }
        });

        dev_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setColorFilter(v, 0xBDBDBD);
                        hideSoftKeyboard(edit_device.this, v);
                        clear_focus();
                        if(is_new){
                            save_new_dev();
                        }else{
                            deleting();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Rect r = new Rect();
                        v.getLocalVisibleRect(r);
                        if (!r.contains((int) event.getX(), (int) event.getY())) {
                            setColorFilter(v, null);
                        }
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        setColorFilter(v, null);
                        break;
                }
                return false;
            }
        });
    }
}
