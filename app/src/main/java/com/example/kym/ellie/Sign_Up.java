package com.example.kym.ellie;

//--------------------------Holds the code for the Sing Up Page--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class Sign_Up extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    NetworkHelper networkHelper = new NetworkHelper();
    Context context = this;

    EditText first_name;
    EditText last_name;
    EditText email;
    EditText password;
    EditText password_confirm;

    TextView first_name_error;
    TextView last_name_error;
    TextView email_error;
    TextView password_error;
    TextView password_confirm_error;

    GradientDrawable first_name_edittext;
    GradientDrawable last_name_edittext;
    GradientDrawable email_edittext;
    GradientDrawable password_edittext;
    GradientDrawable password_confirm_edittext;

    TextView user_sec_word;
    TextView email_sec_word;
    TextView pass_sec_word;

    ImageView user_sec;
    ImageView email_sec;
    ImageView pass_sec;

    GradientDrawable user_stroke;
    GradientDrawable pass_stroke;
    GradientDrawable email_stroke;

    Animation shake;

    //--------------------------Create dialog for account created--------------------------------

    private void show_complete() {

        AlertDialog.Builder signup_complete = new AlertDialog.Builder(this);
        signup_complete.setTitle("Account Created");
        EditText first_name = (EditText) findViewById(R.id.first_name);
        String first_name_value = first_name.getText().toString();
        String ellie_message = "Hello " + first_name_value + ". :) - Ellie";
        signup_complete.setMessage(ellie_message);
        signup_complete.setPositiveButton("GO BACK TO LOGIN",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Intent go_login = new Intent(Sign_Up.this, Login.class);
                        startActivity(go_login);
                        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        finish();
                    }
                });

        AlertDialog helpDialog = signup_complete.create();
        helpDialog.show();
    }

    //--------------------------Create dialog for error when email is already used--------------------------------

    private void show_duplicate() {

        AlertDialog.Builder signup_complete = new AlertDialog.Builder(this);
        signup_complete.setTitle("");
        signup_complete.setMessage("The email you provided has already been used.");
        signup_complete.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog helpDialog = signup_complete.create();
        helpDialog.show();
    }

    //--------------------------Create dialog for failed account creation--------------------------------

    private void show_failed() {

        AlertDialog.Builder signup_complete = new AlertDialog.Builder(this);
        signup_complete.setTitle("Network Error");
        signup_complete.setMessage("Please check if you are connected to the same network as the Rpi.");
        signup_complete.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        signup_complete.setPositiveButton("RETRY",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        signup_submit();
                    }
                });
        AlertDialog helpDialog = signup_complete.create();
        helpDialog.show();
    }

    //--------------------------Create generic loader--------------------------------

    private ProgressDialog create_loader() {

        ProgressDialog dialog = new ProgressDialog(Sign_Up.this);
        dialog.setMessage("Creating Account");
        return dialog;
    }

    //--------------------------Shake animations--------------------------------

    private int shake_error(int error) {

        switch (error){
            case 0:
                return 0;
            case 1:
                user_sec_error();
                return 1;
            case 2:
                email_sec_error();
                return 1;
            case 3:
                user_sec_error();
                email_sec_error();
                return 1;
            case 4:
                pass_sec_error();
                return 1;
            case 5:
                user_sec_error();
                pass_sec_error();
                return 1;
            case 6:
                email_sec_error();
                pass_sec_error();
                return 1;
            case 7:
                user_sec_error();
                email_sec_error();
                pass_sec_error();
                return 1;
        }
        return 1;
    }

    //--------------------------Error Checking--------------------------------

    void user_sec_error(){
        user_sec.startAnimation(shake);
        user_sec.setColorFilter(Color.RED);
        user_sec_word.setTextColor(Color.RED);
        user_stroke.setStroke(3, Color.RED);
    }

    void email_sec_error(){
        email_sec.startAnimation(shake);
        email_sec.setColorFilter(Color.RED);
        email_sec_word.setTextColor(Color.RED);
        email_stroke.setStroke(3, Color.RED);
    }

    void pass_sec_error(){
        pass_sec.startAnimation(shake);
        pass_sec.setColorFilter(Color.RED);
        pass_sec_word.setTextColor(Color.RED);
        pass_stroke.setStroke(3, Color.RED);
    }

    void check_user_sec(){
        if(last_name_error.getText().toString().matches("")&&first_name_error.getText().toString().matches("")){
            user_sec.setColorFilter(Color.parseColor("#00796B"));
            user_sec_word.setTextColor(Color.parseColor("#00796B"));
            user_stroke.setStroke(3, Color.parseColor("#00796B"));
        }
    }

    void check_email_sec(){
        if(email_error.getText().toString().matches("")){
            email_sec.setColorFilter(Color.parseColor("#00796B"));
            email_sec_word.setTextColor(Color.parseColor("#00796B"));
            email_stroke.setStroke(3, Color.parseColor("#00796B"));
        }
    }

    void check_pass_sec(){
        if(password_error.getText().toString().matches("")||password_confirm_error.getText().toString().matches("")){
            pass_sec.setColorFilter(Color.parseColor("#00796B"));
            pass_sec_word.setTextColor(Color.parseColor("#00796B"));
            pass_stroke.setStroke(3, Color.parseColor("#00796B"));
        }
    }

    //--------------------------Keyboard Movements--------------------------------

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus){

            if (v.getId() == R.id.first_name && hasFocus){
                first_name_error.setText("");
                first_name_edittext.setStroke(2, Color.parseColor("#26A69A"));
                check_user_sec();
            }else if(v.getId() == R.id.last_name && hasFocus){
                last_name_error.setText("");
                last_name_edittext.setStroke(2, Color.parseColor("#26A69A"));
                check_user_sec();
            }else if(v.getId() == R.id.email && hasFocus){
                email_error.setText("");
                email_edittext.setStroke(2, Color.parseColor("#26A69A"));
                check_email_sec();
            }else if(v.getId() == R.id.password && hasFocus){
                password_error.setText("");
                password_confirm_error.setText("");
                password_edittext.setStroke(2, Color.parseColor("#26A69A"));
                password_confirm_edittext.setStroke(1, Color.parseColor("#80CBC4"));
                check_pass_sec();
            }else if(v.getId() == R.id.password_confirm && hasFocus){
                password_confirm_error.setText("");
                password_error.setText("");
                password_confirm_edittext.setStroke(2, Color.parseColor("#26A69A"));
                password_edittext.setStroke(1, Color.parseColor("#80CBC4"));
                check_pass_sec();
            }

            if (v.getId() == R.id.first_name && !hasFocus){
                first_name_edittext.setStroke(1, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.last_name && !hasFocus){
                last_name_edittext.setStroke(1, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.email && !hasFocus){
                email_edittext.setStroke(1, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.password && !hasFocus){
                password_edittext.setStroke(1, Color.parseColor("#80CBC4"));
            }else if(v.getId() == R.id.password_confirm && !hasFocus){
                password_confirm_edittext.setStroke(1, Color.parseColor("#80CBC4"));
            }

            if ((v.getId() == R.id.first_name && !hasFocus)||(v.getId() == R.id.last_name && !hasFocus)||(v.getId() == R.id.email && !hasFocus)||(v.getId() == R.id.password && !hasFocus)||(v.getId() == R.id.password_confirm && !hasFocus)) {
                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }else{
                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        }
    }

    //--------------------------Create the account in the RPi--------------------------------

    public void signup_submit(){
        int error = 0;
        final ProgressDialog loader = create_loader();

        String first_name_value = first_name.getText().toString();
        String last_name_value = last_name.getText().toString();
        String email_value = email.getText().toString();
        String password_value = password.getText().toString();
        String password_confirm_value = password_confirm.getText().toString();

        if(first_name_value.matches("")){
            first_name_error.setText("First name is required");
            first_name_edittext.setStroke(2, Color.RED);
            error = error | 1;
        }

        if(last_name_value.matches("")){
            last_name_error.setText("Last name is required");
            last_name_edittext.setStroke(2, Color.RED);
            error = error | 1;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email_value).matches()){
            email_error.setText("Please enter a valid email");
            email_edittext.setStroke(2, Color.RED);
            error = error | 2;
        }

        if(email_value.matches("")){
            email_error.setText("Email is required");
            email_edittext.setStroke(2, Color.RED);
            error = error | 2;
        }


        if(password_value.matches("")&&password_confirm_value.matches("")){
            password_error.setText("Password is required");
            password_confirm_error.setText("No Password Entered");
            password_edittext.setStroke(2, Color.RED);
            password_confirm_edittext.setStroke(2, Color.RED);
            shake_error(error | 4);
        }else if(password_confirm_value.matches("")){
            password_confirm_error.setText("Passwords do not match");
            password_confirm_edittext.setStroke(2, Color.RED);
            shake_error(error | 4);
        }else if(password_value.matches("")){
            password_error.setText("Password is required");
            password_edittext.setStroke(2, Color.RED);
            shake_error(error | 4);
        }else if(!password_value.matches(password_confirm_value)) {
            password_error.setText("Passwords do not match");
            password_confirm_error.setText("Passwords do not match");
            password_edittext.setStroke(2, Color.RED);
            password_confirm_edittext.setStroke(2, Color.RED);
            shake_error(error | 4);
        }else if(shake_error(error) == 0){
            loader.show();
            JSONObject signup = new JSONObject();
            try {
                signup.put("firstname", first_name_value);
                signup.put("lastname", last_name_value);
                signup.put("email", email_value);
                signup.put("password", password_value);
            }catch(JSONException e){
                show_failed();
            }

            networkHelper.post("api/v1/users/save", signup.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            show_failed();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String responseStr = response.body().string();
                    boolean success = false;
                    try {
                        JSONObject info = new JSONObject(responseStr);

                        if (info.has("success")) {
                            success = info.optBoolean("success");
                        }
                        if (success == true) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loader.dismiss();
                                    show_complete();
                                }
                            });
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loader.dismiss();
                                    show_failed();
                                }
                            });
                        }

                    }catch (Exception e){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loader.dismiss();
                                show_failed();
                            }
                        });
                    }
                }
            });

        }
    }

    //--------------------------Reset/Init--------------------------------

    void reset_strokes(){

        user_stroke.setStroke(3, Color.parseColor("#00796B"));
        email_stroke.setStroke(3, Color.parseColor("#00796B"));
        pass_stroke.setStroke(3, Color.parseColor("#00796B"));
        first_name_edittext.setStroke(1, Color.parseColor("#80CBC4"));
        last_name_edittext.setStroke(1, Color.parseColor("#80CBC4"));
        email_edittext.setStroke(1, Color.parseColor("#80CBC4"));
        password_edittext.setStroke(1, Color.parseColor("#80CBC4"));
        password_confirm_edittext.setStroke(1, Color.parseColor("#80CBC4"));

    }

    //--------------------------HGoing back to login--------------------------------

    public boolean onOptionsItemSelected(MenuItem item){

        Intent go_login = new Intent(this, Login.class);
        startActivity(go_login);
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        finish();
        return true;
    }

    //--------------------------More keyboard movements--------------------------------

    private void clearfocus (){
        first_name.clearFocus();
        last_name.clearFocus();
        email.clearFocus();
        password.clearFocus();
        password_confirm.clearFocus();
    }

    public static void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign__up);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Button signup = (Button) findViewById(R.id.sign_up_button);

        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password_confirm = (EditText) findViewById(R.id.password_confirm);

        first_name_error = (TextView)findViewById(R.id.first_name_error);
        last_name_error = (TextView)findViewById(R.id.last_name_error);
        email_error = (TextView)findViewById(R.id.email_error);
        password_error = (TextView)findViewById(R.id.password_error);
        password_confirm_error = (TextView)findViewById(R.id.password_confirm_error);

        LayerDrawable bg;
        bg = (LayerDrawable)first_name.getBackground();
        first_name_edittext = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)last_name.getBackground();
        last_name_edittext = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)email.getBackground();
        email_edittext = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)password.getBackground();
        password_edittext = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);
        bg = (LayerDrawable)password_confirm.getBackground();
        password_confirm_edittext = (GradientDrawable)bg.findDrawableByLayerId(R.id.edittext_bg);

        user_sec_word = (TextView)findViewById(R.id.user_section);
        email_sec_word = (TextView)findViewById(R.id.email_section);
        pass_sec_word = (TextView)findViewById(R.id.lock_section);

        user_sec = (ImageView)findViewById(R.id.user_img);
        email_sec = (ImageView)findViewById(R.id.email_img);
        pass_sec = (ImageView)findViewById(R.id.lock_img);

        shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        bg = (LayerDrawable)user_sec_word.getBackground();
        user_stroke = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);
        bg = (LayerDrawable)email_sec_word.getBackground();
        email_stroke = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);
        bg = (LayerDrawable)pass_sec_word.getBackground();
        pass_stroke = (GradientDrawable)bg.findDrawableByLayerId(R.id.section_bg);

        View.OnFocusChangeListener ofcListener_first_name = new MyFocusChangeListener();
        first_name.setOnFocusChangeListener(ofcListener_first_name);

        View.OnFocusChangeListener ofcListener_last_name = new MyFocusChangeListener();
        last_name.setOnFocusChangeListener(ofcListener_last_name);

        View.OnFocusChangeListener ofcListener_email = new MyFocusChangeListener();
        email.setOnFocusChangeListener(ofcListener_email);

        View.OnFocusChangeListener ofcListener_password = new MyFocusChangeListener();
        password.setOnFocusChangeListener(ofcListener_password);

        View.OnFocusChangeListener ofcListener_password_confirm = new MyFocusChangeListener();
        password_confirm.setOnFocusChangeListener(ofcListener_password_confirm);

        signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setColorFilter(v, 0xBDBDBD);
                        hideSoftKeyboard(Sign_Up.this, v);
                        clearfocus();
                        signup_submit();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Rect r = new Rect();
                        v.getLocalVisibleRect(r);
                        if (!r.contains((int) event.getX(), (int) event.getY())) {
                            setColorFilter(v, null);
                        }
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        setColorFilter(v, null);
                        break;
                }
                return false;
            }

            private void setColorFilter(View v, Integer filter) {
                if (filter == null) v.getBackground().clearColorFilter();
                else {
                    LightingColorFilter darken = new LightingColorFilter(filter, 0x000000);
                    v.getBackground().setColorFilter(darken);
                }
                v.getBackground().invalidateSelf();
            }
        });
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Sign_Up Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.kym.ellie/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        reset_strokes();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Sign_Up Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.kym.ellie/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
