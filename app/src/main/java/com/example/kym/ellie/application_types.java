package com.example.kym.ellie;

//--------------Holds the arrays for application icons, names and an iterator------------------------

public class application_types {
    public static String[] app_typeArray = {
            "Air Conditioning",
            "Air Ioniser",
            "Clothes Dryer",
            "Computer",
            "Dishwasher",
            "Electric Fan",
            "Electric Water Boiler",
            "Exhaust Fan",
            "Game Consoles",
            "Hair Dryer",
            "Hair Iron",
            "Home Entertainment",
            "Home Server",
            "Humidifier",
            "Hygiene Appliance",
            "Induction Cooker",
            "Lighting",
            "Microwave Oven",
            "Mobile Phone",
            "Modem",
            "Oven Toaster",
            "Printer",
            "Projector",
            "Refridgerator",
            "Rice Cooker",
            "Router",
            "Sound System",
            "Television",
            "Vacuum Cleaner",
            "Vehicle",
            "Ventilation",
            "Washing Machine",
            "Water Dispenser"
    };

    private static int[] icons_array = {
            R.drawable.air_conditioner,
            R.drawable.air_ioniser,
            R.drawable.clothes_dryer,
            R.drawable.computer,
            R.drawable.dishwasher,
            R.drawable.electric_fan,
            R.drawable.electric_kettle,
            R.drawable.exhaust_fan,
            R.drawable.game_consoles,
            R.drawable.hair_dryer,
            R.drawable.hair_iron,
            R.drawable.home_entertainment,
            R.drawable.home_server,
            R.drawable.humidifier,
            R.drawable.hygiene_appliance,
            R.drawable.induction_cooker,
            R.drawable.lighting,
            R.drawable.microwave_oven,
            R.drawable.mobile_phone,
            R.drawable.modem,
            R.drawable.oven_toaster,
            R.drawable.printer,
            R.drawable.projector,
            R.drawable.refrigerator,
            R.drawable.rice_cooker,
            R.drawable.router,
            R.drawable.sound_system,
            R.drawable.tv,
            R.drawable.vacuum_cleaner,
            R.drawable.vehicle,
            R.drawable.ventilation,
            R.drawable.washing_machine,
            R.drawable.water_dispenser
    };

    public static int get_icon(String appliance_type){
        int i;
        for(i = 0; i < application_types.app_typeArray.length; i++){
            if(appliance_type.equalsIgnoreCase(application_types.app_typeArray[i])){
                    return application_types.icons_array[i];
            }
        }

        return R.drawable.ellie_emblem_icon;
    }
}
