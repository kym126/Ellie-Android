package com.example.kym.ellie;

//--------------------------Holds the code for dispalying the consumption trend--------------------------------

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;

public class Home_Ellie_Trend extends Fragment implements Home.YourFragmentInterface {

    BarChart chart;
    protected boolean onCreateViewCalled = false;
    TextView title;

    @Override
    public void fragmentBecameVisible() {
        if(onCreateViewCalled) {
            title.setTranslationY(-150f);
            title.animate().translationY(0f).setDuration(750);
            chart.invalidate();
            chart.animateY(2000);
        }
    }

    //--------------------------Create the bar graph--------------------------------

    private void create_chart(){
        XAxis xAxis = chart.getXAxis();
        xAxis.setLabelRotationAngle(45f);

        BarDataSet dataset = new BarDataSet(user_info.init_consumptions_array, "Consumption in Php");
        dataset.setColor(Color.parseColor("#CDDC39"));
        dataset.setValueTextColor(Color.parseColor("#1B5E20"));
        dataset.setValueTextSize(13f);
        dataset.setHighLightColor(Color.parseColor("#006064"));
        BarData set = new BarData(user_info.trend_months, dataset);
        chart.setDescription("");
        chart.setData(set);
        chart.invalidate();
        chart.animateY(2000);
        onCreateViewCalled = true;
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_home__ellie__trend, container, false);

        title = (TextView)v.findViewById(R.id.home_trend_section_title);
        title.setTranslationY(-150f);
        title.animate().translationY(0f).setDuration(750);
        chart = (BarChart)v.findViewById(R.id.trend_chart);
        chart.setDrawGridBackground(false);
        chart.setDrawBarShadow(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);

        create_chart();

        return v;
    }
}

