package com.example.kym.ellie;

//-------------------------DO NOT EDIT for cosmetics only-------------------------------

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class no_swipe_view_pager extends ViewPager {

    public no_swipe_view_pager(Context context) {
        super(context);
    }

    public no_swipe_view_pager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return false;
    }
}