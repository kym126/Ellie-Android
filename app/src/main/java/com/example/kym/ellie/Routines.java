package com.example.kym.ellie;

//--------------------------Holds the code for Routines Fragment--------------------------------

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Routines extends Fragment {

    TextView no_routine;
    ListView routine_list;
    FloatingActionButton add_routine;
    public static ArrayList<Route> Routines = new ArrayList<Route>();
    private ArrayList<Task> task_list = new ArrayList<Task>();
    private ArrayList<View> list = new ArrayList<View>();
    private static final int new_routine = 126, update_routine = 1121;
    DrawerListAdapter adapter;
    boolean has_added = false, is_animating = false;
    int clicked_index;
    View clicked_view;

    //--------------------------Creates the task object--------------------------------

    public static class Task {
        String Device;
        String Status;
        String Time;
        int Icon;
        int Device_ID;

        public Task(String name, String stat, String time, int icon, int id) {
            Device = name;
            Status = stat;
            Time = time;
            Icon = icon;
            Device_ID = id;
        }
    }

    int[] Colors={
            Color.parseColor("#F44336"),
            Color.parseColor("#4CAF50"),
            Color.parseColor("#2196F3"),
            Color.parseColor("#FF5722")
    };

    //--------------------------Creates the Route object--------------------------------

    public static class Route {
        String Name;
        String Task_Num;
        ArrayList<Task> Tasks = new ArrayList<Task>();
        int ID;
        boolean Active;

        public Route(String name, String task_num, int id, ArrayList<Task> task, boolean active) {
            Name = name;
            Task_Num = task_num;
            ID = id;
            for(int i = 0; i < task.size(); i++){
                Tasks.add(new Task(task.get(i).Device, task.get(i).Status, task.get(i).Time, task.get(i).Icon, task.get(i).Device_ID));
            }
            Active = active;
        }
    }

    //--------------------------Handles any resulting activity the redirects here--------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            //if new routine is made

            case(new_routine):
                if (resultCode == Activity.RESULT_OK) {
                    if(!data.getExtras().containsKey("deleted")&&data.getExtras().getBoolean("changed", false)){
                        is_animating = true;
                        no_routine.setVisibility(View.INVISIBLE);
                        final Handler delay = new Handler();

                        Routines.get(Routines.size() - 1).Active = data.getBooleanExtra("active", false);
                        Log.d("params", ""+data.getBooleanExtra("active", false)+" "+data.getExtras().containsKey("active"));
                        delay.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                routine_list.smoothScrollToPositionFromTop(routine_list.getCount(), 0, 500);
                            }
                        }, 700);
                        has_added = true;
                    }else{
                        has_added = false;
                    }
                    adapter.notifyDataSetChanged();
                }
                break;

            //if a routines is updated/deleted

            case(update_routine):
                if (resultCode == Activity.RESULT_OK) {
                    if(data.getExtras().containsKey("deleted")){
                        is_animating = true;
                        final Handler delay1 = new Handler();
                        delay1.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Animation fade = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);
                                fade.setDuration(700);
                                routine_list.getChildAt(clicked_index).setVisibility(View.INVISIBLE);
                                routine_list.getChildAt(clicked_index).startAnimation(fade);
                                final Handler delay2 = new Handler();
                                delay2.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        for(int i = clicked_index; i < list.size(); i++){
                                            list.get(i).animate().translationY(-265f).setDuration(500).start();
                                        }
                                        if (list.size() == 1) {
                                            no_routine.setVisibility(View.VISIBLE);
                                        }
                                        final Handler delay3 = new Handler();
                                        delay3.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                for(int i = clicked_index; i < list.size(); i++){
                                                    list.get(i).setTranslationY(0f);
                                                }
                                                routine_list.getChildAt(clicked_index).setVisibility(View.VISIBLE);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }, 700);
                                    }
                                }, 1200);
                            }
                        }, 700);
                    }else{
                        is_animating = true;
                        final TextView name = (TextView)clicked_view.findViewById(R.id.name);
                        final TextView num_task = (TextView)clicked_view.findViewById(R.id.task_num);
                        final TextView status = (TextView)clicked_view.findViewById(R.id.status);

                        final Animation fade = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
                        fade.setDuration(500);

                        final Animation fade_in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
                        fade_in.setDuration(500);

                        int changed = 0;

                        if(!name.getText().toString().matches(Routines.get(clicked_index).Name)){
                            changed = changed | 1;
                        }

                        if(!num_task.getText().toString().matches(""+Routines.get(clicked_index).Tasks.size()+" tasks")){
                            changed = changed | 2;
                        }

                        if(Routines.get(clicked_index).Active != data.getBooleanExtra("active", false)){
                            changed = changed | 4;
                        }

                        final boolean new_active = data.getBooleanExtra("active", false);
                        final String new_name = Routines.get(clicked_index).Name, new_task = ""+Routines.get(clicked_index).Tasks.size()+" tasks";
                        final int change_val = changed;
                        Routines.get(clicked_index).Task_Num = ""+Routines.get(clicked_index).Tasks.size()+" tasks";
                        Routines.get(clicked_index).Active = data.getBooleanExtra("active", false);
                        if(change_val != 0) {
                            final Handler delay1 = new Handler();
                            delay1.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if((change_val&1) != 0){
                                        name.startAnimation(fade);
                                        name.setVisibility(View.INVISIBLE);
                                    }

                                    if((change_val&2) != 0){
                                        num_task.startAnimation(fade);
                                        num_task.setVisibility(View.INVISIBLE);
                                    }

                                    if((change_val&4) != 0){
                                        if(new_active == true){
                                            status.startAnimation(fade_in);
                                            status.setVisibility(View.VISIBLE);
                                        }else{
                                            status.startAnimation(fade);
                                            final Handler status_delay = new Handler();
                                            status_delay.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    status.setVisibility(View.INVISIBLE);
                                                }
                                            }, 500);
                                        }
                                    }

                                    final Handler delay2 = new Handler();
                                    delay2.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            name.setText(new_name);
                                            num_task.setText(new_task);

                                            if((change_val&1) != 0){
                                                name.startAnimation(fade_in);
                                                name.setVisibility(View.VISIBLE);
                                            }

                                            if((change_val&2) != 0){
                                                num_task.startAnimation(fade_in);
                                                num_task.setVisibility(View.VISIBLE);
                                            }

                                            final Handler delay3 = new Handler();
                                            delay3.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }, 800);
                                        }
                                    }, 800);
                                }
                            }, 700);
                        }else{
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
        }
    }

    //--------------------------Creates the List of routines--------------------------------

    class DrawerListAdapter extends BaseAdapter {

        android.content.Context Context;
        ArrayList<Route> route_array;

        public DrawerListAdapter(Context context, ArrayList<Route> routes) {
            Context = context;
            route_array = routes;
        }

        @Override
        public boolean isEnabled(int position) {
            if (is_animating){
                return false;
            }else{
                return true;
            }
        }

        @Override
        public int getCount() {
            return route_array.size();
        }

        @Override
        public Object getItem(int position) {
            return route_array.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.routine_item, null);
            } else {
                view = convertView;
            }

            TextView name = (TextView)view.findViewById(R.id.name);
            TextView num_task = (TextView)view.findViewById(R.id.task_num);
            TextView status = (TextView)view.findViewById(R.id.status);
            TextView icon = (TextView)view.findViewById(R.id.icon);

            if(route_array.get(position).Active){
                status.setVisibility(View.VISIBLE);
            }else{
                status.setVisibility(View.INVISIBLE);
            }

            int print_pos = position + 1;

            icon.setText(""+print_pos);

            LayerDrawable circle = (LayerDrawable)icon.getBackground();
            GradientDrawable circle_icon = (GradientDrawable)circle.findDrawableByLayerId(R.id.circle_bg);

            name.setText(route_array.get(position).Name);
            num_task.setText(route_array.get(position).Task_Num);

            circle_icon.setColor(Colors[position % 4]);

            if(position == 0){
                list.clear();
            }

            list.add(view);

            if(routine_list.getCount() - 1 == position&&has_added) {
                view.setVisibility(View.INVISIBLE);
                has_added = false;
                final Handler delay = new Handler();
                delay.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(View.VISIBLE);
                        Animation add = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
                        add.setDuration(500);
                        view.startAnimation(add);
                        final Handler delay1 = new Handler();
                        delay1.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                is_animating = false;
                            }
                        }, 700);
                    }
                }, 700);
            }else if(routine_list.getCount() - 1 == position){
                is_animating = false;
            }

            return view;
        }
    }

    //--------------------------Get all the routines form local copy--------------------------------

    private void fetch_routines(){
        for(int i = 0; i < user_info.init_routines.size(); i++){
            Routines.add(user_info.init_routines.get(i));
        }
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.routines,container,false);

        Routines.clear();

        no_routine = (TextView) v.findViewById(R.id.no_routines);
        routine_list =(ListView)v.findViewById(R.id.list_routines);
        add_routine= (FloatingActionButton)v.findViewById(R.id.add_routine);

        add_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent create_routine = new Intent(getActivity(), edit_routines.class);
                create_routine.putExtra("new", 0);
                startActivityForResult(create_routine, new_routine);
                getActivity().overridePendingTransition(R.anim.login_enter, R.anim.ellie_exit);
            }
        });

        fetch_routines();

        adapter = new DrawerListAdapter(getActivity(), Routines);

        routine_list.setAdapter(adapter);

        routine_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.notifyDataSetChanged();
                clicked_view = view;
                clicked_index = position;
                Intent edit_routine = new Intent(getActivity(), edit_routines.class);
                edit_routine.putExtra("routine_name", Routines.get(position).Name);
                edit_routine.putExtra("routine_id", Routines.get(position).ID);
                edit_routine.putExtra("active", Routines.get(position).Active);
                edit_routine.putExtra("position", position);
                startActivityForResult(edit_routine, update_routine);
                getActivity().overridePendingTransition(R.anim.login_enter, R.anim.ellie_exit);
            }
        });

        if(Routines.size() != 0){
            no_routine.setVisibility(View.INVISIBLE);
        }

        return v;
    }
}