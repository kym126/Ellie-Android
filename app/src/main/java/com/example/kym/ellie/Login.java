package com.example.kym.ellie;

//--------------Holds the code for the whole login page--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ActivityInfo;
import android.graphics.LightingColorFilter;
import android.graphics.Rect;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarEntry;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

public class Login extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    NetworkHelper networkHelper = new NetworkHelper();
    Context context = this;
    private ArrayList<Routines.Task> task_list;
    Calendar mcurrentTime = Calendar.getInstance();
    int curr_year = mcurrentTime.get(Calendar.YEAR);
    int curr_month = mcurrentTime.get(Calendar.MONTH);
    int ctr;

    private AlertDialog IPDialog;

    NsdManager.DiscoveryListener mDiscoveryListener;
    NsdManager mNsdManager;
    NsdManager.ResolveListener mResolveListener;
    NsdServiceInfo mService;

    ProgressDialog nsdLoader;

    //---------------Creates the pop-up dialog--------------------------------------------------------------

    private void open_dialog() {
        AlertDialog.Builder ip = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View convertView = inflater.inflate(R.layout.ip_address, null);
        ip.setView(convertView);
        final EditText ip_add = (EditText)convertView.findViewById(R.id.ip_address);

        ip.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        user_info.set_url("http://"+ip_add.getText().toString()+":3000/");
                        hideSoftKeyboard(Login.this, convertView);
                        dialog.dismiss();
                    }
                });

        IPDialog = ip.create() ;
        IPDialog.show();
    }

    //------------------------------------Fetch the devices from RPI--------------------------------

    private void get_devices(final ProgressDialog loader){
        String get_request = "api/v1/devices/user_devices?user_id=" + user_info.getid();
        networkHelper.get(get_request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loader.dismiss();
                show_failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string();
                response.body().close();
                try {
                    int dev_id = 0, dev_code = 0;
                    boolean is_active = false;
                    String dev_name = "", dev_desc = "", dev_app_type = "", dev_brand = "", dev_loc = "";

                    JSONObject results = new JSONObject(response_str);
                    JSONArray devices = results.getJSONArray("results");
                    for (int i = 0; i < devices.length(); i++) {
                        JSONObject device = devices.getJSONObject(i);
                        if (device.has("id")) {
                            dev_id = device.optInt("id");
                        }
                        if (device.has("tedsId")) {
                            dev_code = device.optInt("tedsId");
                        }
                        if (device.has("name")) {
                            dev_name = device.optString("name");
                        }
                        if (device.has("description")) {
                            dev_desc = device.optString("description");
                        }
                        if (device.has("applianceType")) {
                            dev_app_type = device.optString("applianceType");
                        }
                        if (device.has("brandName")) {
                            dev_brand = device.optString("brandName");
                        }
                        if (device.has("location")) {
                            dev_loc = device.optString("location");
                        }
                        if (device.has("active")) {
                            is_active = device.optBoolean("active");
                        }

                        user_info.add_device(dev_name, dev_desc, dev_app_type, dev_brand, dev_loc, application_types.get_icon(dev_app_type), dev_id, dev_code, is_active);
                    }
                    get_routines(loader);
                } catch (Exception e) {
                    get_routines(loader);
                }
            }
        });
    }

    //------------------------------------Fetch the routines from RPI-------------------------------------

    private void get_routines(final ProgressDialog loader){
        String get_request = "api/v1/routines/user_routines?user_id=" + user_info.getid();
        networkHelper.get(get_request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loader.dismiss();
                show_failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string(), name = "";
                int id = 0;
                boolean is_active = false;
                response.body().close();
                try{
                    JSONObject result = new JSONObject(response_str);
                    JSONArray routines  = result.getJSONArray("results");
                    for (int i = 0; i < routines.length(); i++) {
                        task_list = new ArrayList<Routines.Task>();
                        JSONObject routine = routines.getJSONObject(i), device;
                        JSONArray tasks;
                        if(routine.has("id")){
                            id = routine.optInt("id");
                        }
                        if(routine.has("name")){
                            name = routine.optString("name");
                        }
                        if (routine.has("active")) {
                            is_active = routine.optBoolean("active");
                        }

                        if(routine.has("Tasks")){
                            String command = "", dev_name = "", app_type = "", time = "";
                            int dev_id = 0, icon = 0;
                            tasks = routine.optJSONArray("Tasks");
                            for (int j = 0; j < tasks.length(); j++) {
                                JSONObject task = tasks.getJSONObject(j);
                                if(task.has("command")){
                                    command = task.optString("command");
                                }
                                if(task.has("executionTime")){
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a"),  orig = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    Date exec = orig.parse(task.optString("executionTime"));
                                    exec.setTime(exec.getTime() - 14400000);
                                    time = dateFormat.format(exec);
                                }
                                if(task.has("Device")){
                                    device = task.getJSONObject("Device");
                                    if(device.has("id")){
                                        dev_id = device.optInt("id");
                                    }
                                    if(device.has("name")){
                                        dev_name = device.optString("name");
                                    }
                                    if(device.has("applianceType")){
                                        app_type = device.optString("applianceType");
                                    }
                                    icon = application_types.get_icon(app_type);
                                    task_list.add(new Routines.Task(dev_name, command, time, icon, dev_id));
                                }
                            }
                            user_info.add_routine(name, id, task_list, is_active);

                        }
                    }
                    get_monthly_consumptions(loader);
                }catch (Exception e){
                    get_monthly_consumptions(loader);
                }
            }
        });
    }

    //------------------------------------Fetch the per month consumption--------------------------------

    private void get_monthly_consumptions(final ProgressDialog loader){
        String get_request = "api/v1/consumptions/get_cons_for_month?month="+ user_info.months[curr_month].toUpperCase();
        get_request = get_request + "&UserId=" + user_info.getid();
        networkHelper.get(get_request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loader.dismiss();
                show_failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string();
                response.body().close();
                float cons = 0;

                try{
                    JSONObject results = new JSONObject(response_str);
                    JSONObject data = new JSONObject();
                    if(results.has("results")){
                        data = results.optJSONObject("results");
                    }
                    if(data.has("elec_consumption")){
                        cons = (float)data.optDouble("elec_consumption");
                    }
                    user_info.setmonthcons(cons);
                    get_devices_consumptions(loader);
                }catch (Exception e){
                    get_devices_consumptions(loader);
                }
            }
        });
    }

    //------------------------------------Fetch the per device consumption--------------------------------

    private void get_devices_consumptions(final ProgressDialog loader){
        String get_request = "api/v1/consumptions/get_devices_cons_for_month?month=" + user_info.months[curr_month].toUpperCase();
        get_request = get_request + "&UserId=" + user_info.getid();
        networkHelper.get(get_request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loader.dismiss();
                show_failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string();
                response.body().close();
                float elec_cons = 0;
                int dev_id = 0;
                String name = "", app_type = "";

                try{
                    JSONArray results = new JSONArray(response_str);
                    for(int i = 0; i < results.length(); i++){
                        JSONObject device = results.optJSONObject(i);
                        if(device.has("elec_consumption")){
                            elec_cons = (float)device.optDouble("elec_consumption");
                        }
                        if(device.has("Device")){
                            JSONObject dev = device.getJSONObject("Device");
                            if(dev.has("id")){
                                dev_id = dev.optInt("id");
                            }
                            if(dev.has("name")){
                                name = dev.optString("name");
                            }
                            if(dev.has("applianceType")){
                                app_type = dev.optString("applianceType");
                            }
                        }
                        user_info.add_dev_cons(name, app_type, dev_id, elec_cons);
                    }
                    get_trend_consumptions(loader);
                }catch (Exception e){
                    get_trend_consumptions(loader);
                }
            }
        });
    }

    //------------------------------------Fetch the data for trend consumption--------------------------------

    private void get_trend_consumptions(final ProgressDialog loader){
        String get_request = "api/v1/consumptions/get_consumption_trend?year=" + curr_year;
        Log.i("Trend Request", get_request);
        get_request = get_request + "&UserId=" + user_info.getid();
        networkHelper.get(get_request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                loader.dismiss();
                show_failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string();
                Log.i("CT Response", response_str);
                response.body().close();

                try {
                    JSONArray results = new JSONArray(response_str);
                    int i = 0;
                    String starter = "01";
                    if (results.length() >= 5) {
                        i = results.length() - 5;
                    } else {
                        i = 0;
                    }

                    for (int iter = 0, j = 0; i < results.length(); i++, iter++) {
                        JSONObject data = results.getJSONObject(i);

                        if (data.has("elec_consumption")) {
                            user_info.add_trend_data((float) data.getDouble("elec_consumption"), j);
                            j++;
                        }
                        if (data.has("month") && iter == 0) {
                            starter = data.optString("month");
                        }

                    }
                    for (BarEntry be :
                            user_info.init_consumptions_array) {
                        Log.i("Bar Entry", be.toString());
                    }


                    user_info.populate_trend_months(starter);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            Intent home = new Intent(context, Home_Ellie.class);
                            startActivity(home);
                            overridePendingTransition(R.anim.ellie_enter, R.anim.login_leave);
                            finish();
                        }
                    });

                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            Intent home = new Intent(context, Home_Ellie.class);
                            startActivity(home);
                            overridePendingTransition(R.anim.ellie_enter, R.anim.login_leave);
                            finish();
                        }
                    });
                }
            }
        });
    }

    //------------------------------------Create the dialog if something fails--------------------------------

    private void show_failed() {

        AlertDialog.Builder signup_complete = new AlertDialog.Builder(this);
        signup_complete.setTitle("Network Error");
        signup_complete.setMessage("Please check if you are connected to the same network as the Rpi.");
        signup_complete.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        signup_complete.setPositiveButton("RETRY",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        user_info.reset_cred();
                        login_submit();
                    }
                });
        AlertDialog helpDialog = signup_complete.create();
        helpDialog.show();
    }

    //------------------------------------Create loader dialog box--------------------------------

    private ProgressDialog create_loader(String message) {

        ProgressDialog dialog = new ProgressDialog(Login.this);
        dialog.setMessage(message);
        return dialog;
    }

    //------------------------------------Shake Animation--------------------------------

    private void shake_error(ProgressDialog loader) {
        loader.dismiss();
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake_login);
        android.support.design.widget.TextInputLayout email = (android.support.design.widget.TextInputLayout) findViewById(R.id.email_input_layout);
        android.support.design.widget.TextInputLayout password = (android.support.design.widget.TextInputLayout) findViewById(R.id.password_input_layout);
        email.startAnimation(shake);
        password.startAnimation(shake);
    }

    //------------------------------------Transfer to Sign Up page--------------------------------

    public void sign_up(View view) {
        Intent sign_up = new Intent(this, Sign_Up.class);
        startActivity(sign_up);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        finish();
    }

    //------------------------------------Submit credentials and go to home page--------------------------------

    public void login_submit(){
        final ProgressDialog loader = create_loader("Logging In");

        EditText email = (EditText) findViewById(R.id.Email);
        EditText password = (EditText) findViewById(R.id.password);
        String email_value = email.getText().toString();
        String password_value = password.getText().toString();
        if((email_value.matches(""))||(password_value.matches(""))){
            shake_error(loader);
        }else{
            loader.show();
            JSONObject credentials = new JSONObject();
            try {
                credentials.put("email", email_value);
                credentials.put("password", password_value);
            }catch(JSONException e){
                loader.dismiss();
                show_failed();
            }

            networkHelper.post("api/v1/users/login", credentials.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            show_failed();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String responseStr = response.body().string();
                    boolean success = false;
                    String firstname = "", lastname = "", email = "";
                    int id = 0;
                    try {
                        JSONObject info = new JSONObject(responseStr);
                        JSONObject user = new JSONObject();

                        if (info.has("success")) {
                            success = info.optBoolean("success");
                        }
                        if (success == true) {
                            if (info.has("user")) {
                                user = info.optJSONObject("user");
                            }
                            if (user.has("firstname")) {
                                firstname = user.optString("firstname");
                            }
                            if (user.has("lastname")) {
                                lastname = user.optString("lastname");
                            }
                            if (user.has("email")) {
                                email = user.optString("email");
                            }
                            if (user.has("id")) {
                                id = user.optInt("id");
                            }
                            user_info.setfirstname(firstname);
                            user_info.setlastname(lastname);
                            user_info.setemail(email);
                            user_info.setid(id);
                            get_devices(loader);
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    shake_error(loader);
                                }
                            });
                        }
                    } catch (Exception e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loader.dismiss();
                                show_failed();
                            }
                        });
                    }
                }
            });
        }
    }

    //------------------------------------Handles keyboard and cursor movements--------------------------------

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus){

            if ((v.getId() == R.id.Email && !hasFocus)||(v.getId() == R.id.password && !hasFocus)) {

                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }else{
                InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        }
    }

    public static void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    //------------------------------------Auto discover/listen to hub--------------------------------

    public void initializeResolveListener() {
        mResolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails.  Use the error code to d
                Log.e("NSD", "Resolve failed" + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.e("NSD", "Resolve Succeeded. " + serviceInfo);
                mService = serviceInfo;
                int port = mService.getPort();
                InetAddress host = mService.getHost();

                Log.d("NSD-IP",host.getHostAddress());
                user_info.set_url(host.getHostAddress());
                nsdLoader.dismiss();
            }
        };
    }

    private void initializeDiscoveryListener() {
        mNsdManager =  (NsdManager) context.getSystemService(context.NSD_SERVICE);

        // Instantiate a new DiscoveryListener
        mDiscoveryListener = new NsdManager.DiscoveryListener() {

            //  Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                nsdLoader = create_loader("Connecting to hub");
                nsdLoader.show();
                Log.d("NSD", "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found!  Do something with it.
                Log.d("NSD", "Service discovery success" + service);
                if(service.getServiceName().contains("ellie-hub")){
                    mNsdManager.resolveService(service, mResolveListener);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e("NSD", "service lost" + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i("NSD", "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("NSD", "Discovery failed: Error code:" + errorCode);
                mNsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("NSD", "Discovery failed: Error code:" + errorCode);
                mNsdManager.stopServiceDiscovery(this);
            }
        };
    }

    //------------------------------------Main code--------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        initializeDiscoveryListener();
        initializeResolveListener();
        mNsdManager.discoverServices(
                "_http._tcp.", NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
        user_info.set_url("10.0.3.2"); //Comment out if not testing using genymotion
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Button login = (Button)findViewById(R.id.login_button);
        TextView signup = (TextView)findViewById(R.id.Sign_up);

        TextView ucl = (TextView)findViewById(R.id.UCL);


        getSupportActionBar().hide();

        final EditText email = (EditText) findViewById(R.id.Email);
        View.OnFocusChangeListener ofcListener_email = new MyFocusChangeListener();
        email.setOnFocusChangeListener(ofcListener_email);

        final EditText password = (EditText) findViewById(R.id.password);
        View.OnFocusChangeListener ofcListener_password = new MyFocusChangeListener();
        password.setOnFocusChangeListener(ofcListener_password);
        signup.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    sign_up(v);
                }
                return false;
            }
        });

        login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setColorFilter(v, 0xBDBDBD);
                        hideSoftKeyboard(Login.this, v);
                        email.clearFocus();
                        password.clearFocus();
                        login_submit();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Rect r = new Rect();
                        v.getLocalVisibleRect(r);
                        if (!r.contains((int) event.getX(), (int) event.getY())) {
                            setColorFilter(v, null);
                        }
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        setColorFilter(v, null);
                        break;
                }
                return false;
            }

            private void setColorFilter(View v, Integer filter) {
                if (filter == null) v.getBackground().clearColorFilter();
                else {
                    LightingColorFilter darken = new LightingColorFilter(filter, 0x000000);
                    v.getBackground().setColorFilter(darken);
                }
                v.getBackground().invalidateSelf();
            }
        });

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Login Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.kym.ellie/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Login Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.kym.ellie/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
