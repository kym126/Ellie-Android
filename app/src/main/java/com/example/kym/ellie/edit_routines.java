package  com.example.kym.ellie;

//--------------------------Holds the code for the editing routines--------------------------------

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class edit_routines extends AppCompatActivity implements ObservableScrollViewCallbacks {

    private View mFlexibleSpaceView;
    private View mToolbarView;
    private EditText mTitleView;
    private int mFlexibleSpaceHeight;
    private boolean mFabIsShown, is_running = false;
    private FloatingActionButton add_task, play_routine;
    private int mFabMargin;
    private RelativeLayout popup_add_task, popup_play_routine, popup_save_routine, popup_delete_routine;
    private ImageView shadow;
    private View clicked_view;
    private int clicked_index;
    private static final int new_task = 2126, edit_task = 2621;
    private boolean is_new_task = false;
    private NetworkHelper networkhelper = new NetworkHelper();
    private LinearLayout task_list;
    private TextView no_task;
    int position = 0;
    private ArrayList<View> list = new ArrayList<View>();
    boolean is_animating = false, is_changed = false, is_new, did_something = false;
    private String prev_name;
    private int routine_id = 0;
    Calendar mcurrentTime = Calendar.getInstance();
    int curr_year = mcurrentTime.get(Calendar.YEAR);
    int curr_month = mcurrentTime.get(Calendar.MONTH) + 1;
    int curr_day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

    private TextView play_routine_word, delete_routine_word;
    private ImageView play_routine_img, delete_routine_img;

    private ArrayList<Routines.Task> task_copy = new ArrayList<Routines.Task>();

    //-------------------------Create generic loader--------------------------------


    private ProgressDialog create_loader(String message) {

        ProgressDialog dialog = new ProgressDialog(edit_routines.this);
        dialog.setMessage(message);
        return dialog;
    }

    //--------------------------Tells RPI that routine is deleted--------------------------------

    private void delete_routine(){
        final ProgressDialog loader = create_loader("Deleting routine");
        String delete_queue = "api/v1/routines/" + routine_id;
        networkhelper.delete(delete_queue, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_routines.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        delete_routine();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        Routines.Routines.remove(position);
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("deleted", true);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                    }
                });
            }
        });
    }

    //--------------------------Create dialog for a deleted routine--------------------------------

    private void deleting() {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);

        exit.setTitle("");
        if(mTitleView.getText().toString().matches("")){
            exit.setMessage("Are you sure you want to delete the routine \"" + prev_name + "\"?");
        }else {
            exit.setMessage("Are you sure you want to delete the routine \"" + mTitleView.getText().toString() + "\"?");
        }
        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        delete_routine();
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }

    //--------------------------Create dialog for saved routine--------------------------------

    private void saved_dialog(String message) {
        AlertDialog.Builder save = new AlertDialog.Builder(this);
        save.setTitle("");
        save.setMessage(message);
        final AlertDialog saveDialog = save.create();
        saveDialog.show();
        final Handler delay = new Handler();
        delay.postDelayed(new Runnable() {
            @Override
            public void run() {
                saveDialog.dismiss();
            }
        }, 1000);
    }

    //--------------------------Create dialog for exiting page--------------------------------

    private void exiting(String message) {
        AlertDialog.Builder exit = new AlertDialog.Builder(this);
        exit.setTitle("");
        exit.setMessage(message);

        exit.setPositiveButton("No",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        exit.setNegativeButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(is_new) {
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra("changed", is_changed);
                            resultIntent.putExtra("active", is_running);
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        }else{
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra("changed", is_changed);
                            resultIntent.putExtra("active", is_running);
                            setResult(Activity.RESULT_OK, resultIntent);
                            finish();
                            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
                        }
                    }
                });
        AlertDialog ExitDialog = exit.create();
        ExitDialog.show();
    }

    //--------------------------Create the functioanlities of buttons on nav bar--------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            if(is_new && !is_changed){
                exiting("Stop creating this routine?");
            }else if(did_something||!mTitleView.getText().toString().matches(prev_name)) {
                if(mTitleView.getText().toString().matches("")){
                    exiting("Discard all changes made to the routine \"" +prev_name+ "\"?");
                }else{
                    exiting("Discard all changes made to the routine \"" +mTitleView.getText().toString()+ "\"?");
                }
            }else if(!did_something){
                Intent resultIntent = new Intent();
                resultIntent.putExtra("changed", is_changed);
                resultIntent.putExtra("active", is_running);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    //--------------------------Populate the tasks on the routine-------------------------------

    private void populate_lilayout(){
        task_list = (LinearLayout)findViewById(R.id.body);
        LayoutInflater inflater = getLayoutInflater();
        TextView dev_name, status, time;
        ImageView icon;

        task_list.removeAllViews();
        list.clear();

        if(task_copy.size() == 0){
            task_list.setGravity(Gravity.CENTER);
            task_list.addView(no_task);
            play_routine_word.setTextColor(Color.parseColor("#80CBC4"));
            play_routine_img.setColorFilter(Color.parseColor("#80CBC4"));
        }else{
            task_list.setGravity(Gravity.NO_GRAVITY);
            if(!is_new || is_new && is_changed) {
                play_routine_word.setTextColor(Color.parseColor("#FFFFFF"));
                play_routine_img.setColorFilter(Color.parseColor("#FFFFFF"));
            }
        }

        LayerDrawable bg;

        for(int i = 0; i < task_copy.size(); i++){
            final View view = inflater.inflate(R.layout.task_item_layout, null);
            task_list.addView(view, i);
            list.add(view);
            dev_name = (TextView)view.findViewById(R.id.device_name);
            status = (TextView)view.findViewById(R.id.status_name);
            time = (TextView)view.findViewById(R.id.time);
            icon = (ImageView)view.findViewById(R.id.icon);

            dev_name.setText(task_copy.get(i).Device);
            status.setText(task_copy.get(i).Status);
            time.setText(task_copy.get(i).Time);
            icon.setImageDrawable(getResources().getDrawable(task_copy.get(i).Icon));

            final int icon_pass  = task_copy.get(i).Icon;
            final String name = task_copy.get(i).Device;
            final String time_val = task_copy.get(i).Time;
            final String Status = task_copy.get(i).Status;

            bg = (LayerDrawable)status.getBackground();
            GradientDrawable bg_status = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);

            if(status.getText().toString().matches("ON")){
                bg_status.setColor(Color.parseColor("#8BC34A"));
            }else{
                bg_status.setColor(Color.parseColor("#F44336"));
            }

            ImageView to_click = (ImageView)view.findViewById(R.id.imageView);

            final int clicked = i;
            to_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!is_animating) {
                        if(is_running) {
                            AlertDialog.Builder exit = new AlertDialog.Builder(edit_routines.this);

                            exit.setTitle("");
                            exit.setMessage("Doing this action would stop the routine from running. Are you sure you want to continue?");
                            exit.setPositiveButton("No",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            exit.setNegativeButton("Yes",
                                    new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            stop_routine();
                                            clicked_view = view;
                                            clicked_index = clicked;
                                            Intent create_task = new Intent(edit_routines.this, new_task.class);
                                            create_task.putExtra("device_name", name);
                                            create_task.putExtra("time", time_val);
                                            create_task.putExtra("icon", icon_pass);
                                            if (Status.matches("ON")) {
                                                create_task.putExtra("command", true);
                                            } else {
                                                create_task.putExtra("command", false);
                                            }
                                            startActivityForResult(create_task, edit_task);
                                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                        }
                                    });
                            AlertDialog ExitDialog = exit.create();
                            ExitDialog.show();
                        }else{
                            clicked_view = view;
                            clicked_index = clicked;
                            Intent create_task = new Intent(edit_routines.this, new_task.class);
                            create_task.putExtra("device_name", name);
                            create_task.putExtra("time", time_val);
                            create_task.putExtra("icon", icon_pass);
                            if (Status.matches("ON")) {
                                create_task.putExtra("command", true);
                            } else {
                                create_task.putExtra("command", false);
                            }
                            startActivityForResult(create_task, edit_task);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        }
                    }
                }
            });
            if(is_new_task == true && i == task_copy.size() - 1){
                view.setVisibility(View.INVISIBLE);
                final ObservableScrollView scroll = (ObservableScrollView)findViewById(R.id.scroll);
                scroll.post(new Runnable() {
                    @Override
                    public void run() {
                        scroll.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
                final Handler delay = new Handler();
                delay.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setVisibility(View.VISIBLE);
                        Animation add = AnimationUtils.loadAnimation(edit_routines.this, android.R.anim.slide_in_left);
                        add.setDuration(500);
                        view.startAnimation(add);
                    }
                }, 700);
                is_new_task = false;
            }
        }
    }

    //--------------------------Catch every activity that will be redirected here--------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            //new task is made

            case(new_task):
                if (resultCode == Activity.RESULT_OK) {
                    String name, status, time;
                    int icon, id;
                    name = data.getStringExtra("dev_name");
                    status = data.getStringExtra("command");
                    time = data.getStringExtra("time");
                    icon = data.getIntExtra("icon", R.drawable.ellie_emblem_icon);
                    is_new_task = true;
                    task_copy.add(new Routines.Task(name, status, time, icon, Devices.get_dev_id(name)));
                    populate_lilayout();
                    did_something = true;
                }
                break;

            //a task is updated/deleted
            case(edit_task):
                if (resultCode == Activity.RESULT_OK) {
                    if(data.getBooleanExtra("deleted", false)){
                        did_something = true;
                        is_animating = true;
                        final Handler delay = new Handler();
                        delay.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                clicked_view.setVisibility(View.INVISIBLE);
                                Animation add = AnimationUtils.loadAnimation(edit_routines.this, android.R.anim.slide_out_right);
                                add.setDuration(500);
                                clicked_view.startAnimation(add);
                                final Handler delay2 = new Handler();
                                delay2.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        task_copy.remove(clicked_index);
                                        for(int i = clicked_index; i <= task_copy.size(); i++){
                                            list.get(i).animate().translationY(-650f).setDuration(1000).start();
                                        }
                                        if(clicked_index == task_copy.size()){
                                            is_animating = false;
                                            populate_lilayout();
                                        }
                                        final Handler delay3 = new Handler();
                                        delay3.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(clicked_index != task_copy.size()){
                                                    is_animating = false;
                                                    populate_lilayout();
                                                }
                                            }
                                        }, 1000);
                                    }
                                }, 1200);
                            }
                        }, 700);
                    }else if(data.getBooleanExtra("changed", false)){
                        did_something = true;
                        is_animating = true;
                        final String name, status, time, new_name, new_status, new_time;
                        final int new_icon;
                        int changed = 0;

                        name = task_copy.get(clicked_index).Device;
                        status = task_copy.get(clicked_index).Status;
                        time = task_copy.get(clicked_index).Time;

                        new_name = data.getStringExtra("dev_name");
                        new_status = data.getStringExtra("command");
                        new_time = data.getStringExtra("time");
                        new_icon = data.getIntExtra("icon", R.drawable.ellie_emblem_icon);

                        if(!name.matches(new_name)){
                            changed  = changed | 1;
                        }

                        if(!status.matches(new_status)){
                            changed  = changed | 2;
                        }

                        if(!time.matches(new_time)){
                            changed  = changed | 4;
                        }

                        if(changed != 0) {

                            final TextView name_ui = (TextView) clicked_view.findViewById(R.id.device_name);
                            final TextView time_ui = (TextView) clicked_view.findViewById(R.id.time);
                            final TextView status_ui = (TextView) clicked_view.findViewById(R.id.status_name);
                            final ImageView iconimg = (ImageView) clicked_view.findViewById(R.id.icon);

                            final Animation fade = AnimationUtils.loadAnimation(edit_routines.this, android.R.anim.fade_out);
                            fade.setDuration(500);

                            final Animation fade_in = AnimationUtils.loadAnimation(edit_routines.this, android.R.anim.fade_in);
                            fade_in.setDuration(500);

                            task_copy.get(clicked_index).Device = new_name;
                            task_copy.get(clicked_index).Status = new_status;
                            task_copy.get(clicked_index).Time = new_time;
                            task_copy.get(clicked_index).Icon = new_icon;
                            task_copy.get(clicked_index).Device_ID = Devices.get_dev_id(new_name);

                            final int change_val = changed;
                            final Handler delay1 = new Handler();
                            delay1.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if((change_val&1) != 0){
                                        name_ui.startAnimation(fade);
                                        name_ui.setVisibility(View.INVISIBLE);
                                        iconimg.startAnimation(fade);
                                        iconimg.setVisibility(View.INVISIBLE);
                                    }

                                    if((change_val&2) != 0){
                                        status_ui.animate().cancel();
                                        status_ui.animate().scaleY(0).scaleX(0).setDuration(500).start();
                                    }

                                    if((change_val&4) != 0){
                                        time_ui.startAnimation(fade);
                                        time_ui.setVisibility(View.INVISIBLE);
                                    }

                                    final Handler delay2 = new Handler();
                                    delay2.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            name_ui.setText(new_name);
                                            status_ui.setText(new_status);
                                            time_ui.setText(new_time);
                                            iconimg.setImageDrawable(getResources().getDrawable(new_icon));
                                            LayerDrawable bg = (LayerDrawable)status_ui.getBackground();
                                            GradientDrawable bg_status = (GradientDrawable)bg.findDrawableByLayerId(R.id.circle_bg);

                                            if(new_status.matches("ON")){
                                                bg_status.setColor(Color.parseColor("#8BC34A"));
                                            }else{
                                                bg_status.setColor(Color.parseColor("#F44336"));
                                            }

                                            if((change_val&1) != 0){
                                                name_ui.startAnimation(fade_in);
                                                name_ui.setVisibility(View.VISIBLE);
                                                iconimg.startAnimation(fade_in);
                                                iconimg.setVisibility(View.VISIBLE);
                                            }

                                            if((change_val&2) != 0){
                                                status_ui.animate().cancel();
                                                status_ui.animate().scaleY(1).scaleX(1).setDuration(500).start();
                                            }

                                            if((change_val&4) != 0){
                                                time_ui.startAnimation(fade_in);
                                                time_ui.setVisibility(View.VISIBLE);
                                            }

                                            final Handler delay3 = new Handler();
                                            delay3.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    populate_lilayout();
                                                    is_animating = false;
                                                }
                                            }, 1000);
                                        }
                                    }, 1000);
                                }
                            }, 700);
                        }
                    }
                }
        }
    }

    //--------------------------Holds the animation for scrolling--------------------------------

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        updateFlexibleSpaceText(scrollY);
        // Translate FAB
        int maxFabTranslationY = mFlexibleSpaceHeight + getActionBarSize() - add_task.getHeight() / 2  - mFabMargin/3;

        float fabTranslationY = ScrollUtils.getFloat(
                -scrollY + mFlexibleSpaceHeight + getActionBarSize() - add_task.getHeight() / 2  - mFabMargin/3,
                getActionBarSize()- add_task.getHeight()/2 - mFabMargin/3,
                maxFabTranslationY);

        float shadowTranslationY = ScrollUtils.getFloat(
                -scrollY + mFlexibleSpaceHeight,
                0,
                mFlexibleSpaceHeight + getActionBarSize());

        ViewHelper.setTranslationY(shadow, shadowTranslationY);
        ViewHelper.setTranslationY(add_task, fabTranslationY);

        LayerDrawable bg;
        bg = (LayerDrawable)mTitleView.getBackground();
        GradientDrawable bg_name = (GradientDrawable)bg.findDrawableByLayerId(R.id.routine_name_bg);


        if(fabTranslationY >= maxFabTranslationY*9/10){
            show_popup();
            hideFab();
            mTitleView.setFocusable(true);
            mTitleView.setFocusableInTouchMode(true);
            bg_name.setStroke(4, Color.parseColor("#ffffff"));
        }else{
            hide_popup();
            showFab();
            mTitleView.setFocusable(false);
            bg_name.setStroke(4, getResources().getColor(R.color.teal));
        }
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    private void updateFlexibleSpaceText(final int scrollY) {
        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY );
        int adjustedScrollY = (int) ScrollUtils.getFloat(scrollY, 0, mFlexibleSpaceHeight);
        float maxScale = (float) (mFlexibleSpaceHeight*3/4 - mToolbarView.getHeight()) / mToolbarView.getHeight();
        float scale = maxScale * ((float) mFlexibleSpaceHeight - adjustedScrollY) / mFlexibleSpaceHeight;

        ViewHelper.setPivotX(mTitleView, 0);
        ViewHelper.setPivotY(mTitleView, 0);
        ViewHelper.setScaleX(mTitleView, 1 + scale);
        ViewHelper.setScaleY(mTitleView, 1 + scale);
        int maxTitleTranslationY = mToolbarView.getHeight() + mFlexibleSpaceHeight/4 - (int) (mTitleView.getHeight() * (1 + scale));
        int titleTranslationY = (int) (maxTitleTranslationY * ((float) mFlexibleSpaceHeight - adjustedScrollY) / mFlexibleSpaceHeight);
        ViewHelper.setTranslationY(mTitleView, titleTranslationY);

    }

    private void showFab() {
        if (!mFabIsShown) {
            add_task.setVisibility(View.VISIBLE);
            add_task.animate().cancel();
            add_task.animate().scaleX(1).scaleY(1).setDuration(200).start();
            if(!is_new || is_new && is_changed) {
                play_routine.setVisibility(View.VISIBLE);
                play_routine.animate().cancel();
                play_routine.animate().scaleX(1).scaleY(1).setDuration(200).start();
            }
            mFabIsShown = true;
        }
    }

    //--------------------------Holds animation for floating action buttons--------------------------------

    private void show_popup() {
        popup_add_task.setVisibility(View.VISIBLE);
        popup_delete_routine.setVisibility(View.VISIBLE);
        popup_save_routine.setVisibility(View.VISIBLE);
        popup_play_routine.setVisibility(View.VISIBLE);
        popup_add_task.animate().cancel();
        popup_add_task.animate().scaleX(1).scaleY(1).setDuration(200).start();
        popup_delete_routine.animate().cancel();
        popup_delete_routine.animate().scaleX(1).scaleY(1).setDuration(200).start();
        popup_save_routine.animate().cancel();
        popup_save_routine.animate().scaleX(1).scaleY(1).setDuration(200).start();
        popup_play_routine.animate().cancel();
        popup_play_routine.animate().scaleX(1).scaleY(1).setDuration(200).start();
    }

    private void hide_popup() {
        popup_add_task.setVisibility(View.INVISIBLE);
        popup_delete_routine.setVisibility(View.INVISIBLE);
        popup_save_routine.setVisibility(View.INVISIBLE);
        popup_play_routine.setVisibility(View.INVISIBLE);
        popup_add_task.animate().cancel();
        popup_add_task.animate().scaleX(0).scaleY(0).setDuration(200).start();
        popup_delete_routine.animate().cancel();
        popup_delete_routine.animate().scaleX(0).scaleY(0).setDuration(200).start();
        popup_save_routine.animate().cancel();
        popup_save_routine.animate().scaleX(0).scaleY(0).setDuration(200).start();
        popup_play_routine.animate().cancel();
        popup_play_routine.animate().scaleX(0).scaleY(0).setDuration(200).start();
    }

    private void hideFab() {
        if (mFabIsShown) {
            add_task.animate().cancel();
            add_task.animate().scaleX(0).scaleY(0).setDuration(200).start();
            play_routine.animate().cancel();
            play_routine.animate().scaleX(0).scaleY(0).setDuration(200).start();
            final Handler delay = new Handler();
            delay.postDelayed(new Runnable() {
                @Override
                public void run() {
                    add_task.setVisibility(View.INVISIBLE);
                    play_routine.setVisibility(View.INVISIBLE);
                }
            }, 300);
            mFabIsShown = false;
        }
    }

    //--------------------------Fetch the tasks in the routine--------------------------------

    private void fetch_curr_tasks(){
        for(int i = 0; i < Routines.Routines.get(position).Tasks.size(); i++){
            task_copy.add(new Routines.Task(Devices.get_dev_name(Routines.Routines.get(position).Tasks.get(i).Device_ID), Routines.Routines.get(position).Tasks.get(i).Status, Routines.Routines.get(position).Tasks.get(i).Time, Routines.Routines.get(position).Tasks.get(i).Icon, Routines.Routines.get(position).Tasks.get(i).Device_ID));
        }
    }

    //--------------------------Tells the RPI that routine is stopped--------------------------------

    private void stop_routine(){
        play_routine.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#4CAF50")));
        play_routine.setImageDrawable(getResources().getDrawable(R.drawable.play_routine));
        play_routine_img.setImageDrawable(getResources().getDrawable(R.drawable.play_routine));
        play_routine_word.setText("Play");
        is_running = false;
        final ProgressDialog loader = create_loader("Stopping routine");
        String execute_queue = "api/v1/routines/"+routine_id+"/stop";
        networkhelper.post(execute_queue, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_routines.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        stop_routine();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                    }
                });
            }
        });
    }

    //--------------------------Tells Rpi that routine is made--------------------------------


    private void create_routine(){
        JSONObject new_routine = new JSONObject();
        JSONObject routine_params = new JSONObject();
        JSONArray tasks_array = new JSONArray();
        JSONObject task_params;
        final ProgressDialog loader = create_loader("Creating routine");
        try{
            routine_params.put("name", mTitleView.getText().toString());
            routine_params.put("UserId", user_info.getid());
            new_routine.put("routine", routine_params);
            for(int i = 0; i < task_copy.size(); i++){
                task_params = new JSONObject();
                task_params.put("command", task_copy.get(i).Status);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                Date exec_time = dateFormat.parse(curr_year+"-"+curr_month+"-"+curr_day+" "+task_copy.get(i).Time);
                task_params.put("executionTime", exec_time);
                task_params.put("index", i);
                task_params.put("DeviceId", task_copy.get(i).Device_ID);
                tasks_array.put(task_params);
            }
            new_routine.put("tasks", tasks_array);
        }catch(Exception e){
        }

        networkhelper.post("api/v1/routines/", new_routine.toString(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_routines.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        create_routine();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String response_str = response.body().string();

                try {
                    JSONObject result = new JSONObject(response_str);
                    if (result.has("RoutineId")) {
                        routine_id = result.getInt("RoutineId");
                    } else {
                        routine_id = 0;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loader.dismiss();
                            saved_dialog("Created routine \"" + mTitleView.getText().toString() + "\"");
                        }
                    });
                } catch (Exception e) {

                }

            }
        });
    }

    //--------------------------Tells Rpi that routine is started--------------------------------

    private void play_routine(){
        final ProgressDialog loader = create_loader("Starting routine");
        String execute_queue = "api/v1/routines/"+routine_id+"/execute";
        networkhelper.post(execute_queue, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_routines.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        play_routine();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        saved_dialog("Started execution of \""+prev_name+"\"");
                    }
                });
            }
        });
    }

    //--------------------------Tells rpi that routine is saved and updated--------------------------------

    private void update_routine(){
        JSONObject new_routine = new JSONObject();
        JSONObject routine_params = new JSONObject();
        JSONArray tasks_array = new JSONArray();
        JSONObject task_params;
        final ProgressDialog loader = create_loader("Updating routine");

        try{
            routine_params.put("name", mTitleView.getText().toString());
            routine_params.put("UserId", user_info.getid());
            new_routine.put("routine", routine_params);
            for(int i = 0; i < task_copy.size(); i++){
                task_params = new JSONObject();
                task_params.put("command", task_copy.get(i).Status);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
                Date exec_time = dateFormat.parse(curr_year+"-"+curr_month+"-"+curr_day+" "+task_copy.get(i).Time);
                task_params.put("executionTime", exec_time);
                task_params.put("index", i);
                task_params.put("DeviceId", task_copy.get(i).Device_ID);
                tasks_array.put(task_params);
            }
            Log.d("array", tasks_array.toString());
            new_routine.put("tasks", tasks_array);
        }catch(Exception e){
        }

        networkhelper.put("api/v1/routines/" + routine_id, new_routine.toString(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        AlertDialog.Builder save_device = new AlertDialog.Builder(edit_routines.this);
                        save_device.setTitle("Network Error");
                        save_device.setMessage("Please check if you are connected to the same network as the Rpi.");
                        save_device.setNeutralButton("CANCEL",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        save_device.setPositiveButton("RETRY",
                                new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        update_routine();
                                    }
                                });
                        AlertDialog helpDialog = save_device.create();
                        helpDialog.show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loader.dismiss();
                        saved_dialog("Saved routine \""+mTitleView.getText().toString()+"\"");
                    }
                });
            }
        });
    }

    //--------------------------Holds the main code--------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_routines);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        no_task = (TextView)findViewById(R.id.no_tasks);
        add_task = (FloatingActionButton)findViewById(R.id.add_task);
        play_routine = (FloatingActionButton)findViewById(R.id.play_routine);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        mTitleView = (EditText) findViewById(R.id.routine_name);
        popup_add_task = (RelativeLayout)findViewById(R.id.popup_add_task);
        popup_play_routine = (RelativeLayout)findViewById(R.id.popup_play_routine);
        popup_save_routine = (RelativeLayout)findViewById(R.id.popup_save_routine);
        popup_delete_routine = (RelativeLayout)findViewById(R.id.popup_delete_routine);
        setTitle(null);
        mToolbarView = findViewById(R.id.toolbar);
        mFabMargin = getResources().getDimensionPixelSize(R.dimen.margin_standard);
        shadow = (ImageView) findViewById(R.id.shadow);
        play_routine_word = (TextView)findViewById(R.id.img_play_routine_word);
        play_routine_img = (ImageView)findViewById(R.id.img_play_routine);
        delete_routine_word = (TextView)findViewById(R.id.img_delete_routine_word);
        delete_routine_img = (ImageView)findViewById(R.id.img_delete_routine);

        final ObservableScrollView scrollView = (ObservableScrollView) findViewById(R.id.scroll);
        scrollView.setScrollViewCallbacks(this);

        if(getIntent().getExtras().containsKey("routine_name")) {
            mTitleView.setText(getIntent().getExtras().getString("routine_name"));
            position = getIntent().getExtras().getInt("position");
            fetch_curr_tasks();
            populate_lilayout();
            is_new = false;
            prev_name = getIntent().getExtras().getString("routine_name");
            is_running = getIntent().getExtras().getBoolean("active");
            routine_id = getIntent().getExtras().getInt("routine_id");
        }else{
            play_routine_word.setTextColor(Color.parseColor("#80CBC4"));
            play_routine_img.setColorFilter(Color.parseColor("#80CBC4"));
            delete_routine_word.setTextColor(Color.parseColor("#80CBC4"));
            delete_routine_img.setColorFilter(Color.parseColor("#80CBC4"));
            is_new = true;
        }

        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_height);
        int flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + getActionBarSize();

        findViewById(R.id.body).setPadding(0, flexibleSpaceAndToolbarHeight, 0, 0);
        mFlexibleSpaceView.getLayoutParams().height = flexibleSpaceAndToolbarHeight;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width_phone = size.x;

        popup_add_task.getLayoutParams().width = (width_phone-20)/4;
        popup_play_routine.getLayoutParams().width = (width_phone - 20) / 4;
        popup_save_routine.getLayoutParams().width = (width_phone - 20) / 4;
        popup_delete_routine.getLayoutParams().width = (width_phone - 20) / 4;
        popup_play_routine.setTranslationY(-10f);

        if(is_running){
            play_routine.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#F44336")));
            play_routine.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
            play_routine_img.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
            play_routine_word.setText("Stop");

        }else{
            play_routine.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#4CAF50")));
            play_routine.setImageDrawable(getResources().getDrawable(R.drawable.play_routine));
            play_routine_img.setImageDrawable(getResources().getDrawable(R.drawable.play_routine));
            play_routine_word.setText("Play");
        }

        ScrollUtils.addOnGlobalLayoutListener(mTitleView, new Runnable() {
            @Override
            public void run() {
                updateFlexibleSpaceText(scrollView.getCurrentScrollY());
            }
        });

        mTitleView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (mTitleView.hasFocus()) {
                    mTitleView.setHint("");
                } else {
                    mTitleView.setHint("Routine Name");
                }
            }
        });

        popup_add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_running) {
                    AlertDialog.Builder exit = new AlertDialog.Builder(edit_routines.this);

                    exit.setTitle("");
                    exit.setMessage("Doing this action would stop the routine from running. Are you sure you want to continue?");
                    exit.setPositiveButton("No",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    exit.setNegativeButton("Yes",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    stop_routine();
                                    Intent create_task = new Intent(edit_routines.this, new_task.class);
                                    create_task.putExtra("new", 0);
                                    startActivityForResult(create_task, new_task);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                }
                            });
                    AlertDialog ExitDialog = exit.create();
                    ExitDialog.show();
                } else {
                    Intent create_task = new Intent(edit_routines.this, new_task.class);
                    create_task.putExtra("new", 0);
                    startActivityForResult(create_task, new_task);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                }
            }
        });

        add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_running) {
                    AlertDialog.Builder exit = new AlertDialog.Builder(edit_routines.this);

                    exit.setTitle("");
                    exit.setMessage("Doing this action would stop the routine from running. Are you sure you want to continue?");
                    exit.setPositiveButton("No",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    exit.setNegativeButton("Yes",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    stop_routine();
                                    Intent create_task = new Intent(edit_routines.this, new_task.class);
                                    create_task.putExtra("new", 0);
                                    startActivityForResult(create_task, new_task);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                                }
                            });
                    AlertDialog ExitDialog = exit.create();
                    ExitDialog.show();
                } else {
                    Intent create_task = new Intent(edit_routines.this, new_task.class);
                    create_task.putExtra("new", 0);
                    startActivityForResult(create_task, new_task);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                }
            }
        });

        popup_play_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!is_new || is_new && is_changed) && (task_copy.size() != 0)) {
                    if (!is_running) {
                        //play
                        play_routine.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#D32F2F")));
                        play_routine.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
                        play_routine_img.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
                        play_routine_word.setText("Stop");
                        is_running = true;
                        play_routine();
                    } else {
                        stop_routine();
                        saved_dialog("Stopped execution of \""+prev_name+"\"");
                    }
                }
            }
        });

        play_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!is_new || is_new && is_changed) && (task_copy.size() != 0)) {
                    if (!is_running) {
                        //play
                        play_routine.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#D32F2F")));
                        play_routine.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
                        play_routine_img.setImageDrawable(getResources().getDrawable(R.drawable.stop_routine));
                        play_routine_word.setText("Stop");
                        is_running = true;
                        play_routine();
                    } else {
                        stop_routine();
                        saved_dialog("Stopped execution of \""+prev_name+"\"");
                    }
                }
            }
        });

        popup_delete_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_new || is_new && is_changed) {
                    deleting();
                }
            }
        });

        popup_save_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save
                if (mTitleView.getText().toString().matches("")) {
                    Animation shake = AnimationUtils.loadAnimation(edit_routines.this, R.anim.routine_name_shake);
                    mTitleView.startAnimation(shake);
                } else {
                    if (is_new && !is_changed) {
                        create_routine();
                        Routines.Routines.add(new Routines.Route(mTitleView.getText().toString(), "" + task_copy.size() + " tasks", routine_id, task_copy, is_running));
                        position = Routines.Routines.size() - 1;
                    } else {
                        update_routine();
                    }
                    is_changed = true;
                    did_something = false;
                    if (task_copy.size() != 0) {
                        play_routine_word.setTextColor(Color.parseColor("#FFFFFF"));
                        play_routine_img.setColorFilter(Color.parseColor("#FFFFFF"));
                    }
                    delete_routine_word.setTextColor(Color.parseColor("#FFFFFF"));
                    delete_routine_img.setColorFilter(Color.parseColor("#FFFFFF"));
                    Routines.Routines.get(position).Name = mTitleView.getText().toString();
                    Routines.Routines.get(position).Tasks.clear();
                    for (int i = 0; i < task_copy.size(); i++) {
                        Routines.Routines.get(position).Tasks.add(new Routines.Task(task_copy.get(i).Device, task_copy.get(i).Status, task_copy.get(i).Time, task_copy.get(i).Icon, task_copy.get(i).Device_ID));
                    }
                    prev_name = mTitleView.getText().toString();
                }
            }
        });

        shadow.setTranslationY(mFlexibleSpaceHeight);
        LayerDrawable bg;
        bg = (LayerDrawable)mTitleView.getBackground();
        GradientDrawable bg_name = (GradientDrawable)bg.findDrawableByLayerId(R.id.routine_name_bg);
        bg_name.setStroke(4, Color.parseColor("#ffffff"));
    }
}
