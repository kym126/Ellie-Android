package com.example.kym.ellie;

//--------------------------Holds the different information whena user logs in--------------------------------
//--------------------------HSaves a local opy of everything--------------------------------

import android.app.Application;
import android.util.Log;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class user_info extends Application{

    private static String[] month_val = {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"
    };

    public static String[] months = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };

    public static ArrayList<Devices.Device> init_devices = new ArrayList<Devices.Device>();

    public static ArrayList<Routines.Route> init_routines = new ArrayList<Routines.Route>();

    public static ArrayList<BarEntry> init_consumptions_array = new ArrayList<BarEntry>();

    public static ArrayList<Home_Ellie_Devices.dev_cons> init_dev_cons = new ArrayList<Home_Ellie_Devices.dev_cons>();

    public static String[] trend_months = new String[5];

    private static String firstname, lastname, email, starting_url;
    private static int id = 0;
    private static float monthly_cons = 0, total_cons = 0;

    public static String getfirstname() {
        return firstname;
    }

    public static void setfirstname(String info) {
        firstname = info;
    }

    public static int getid() {
        return id;
    }

    public static void setid(int info) {
        id = info;
    }

    public static void setmonthcons(float info) {
        monthly_cons = info;
    }

    public static float getmonthcons() {
        return monthly_cons;
    }

    public static float gettotaldevcons() {
        return total_cons;
    }

    public static String getlastname() {
        return lastname;
    }

    public static void setlastname(String info) {
        lastname = info;
    }

    public static String getemail() {
        return email;
    }

    public static void setemail(String info) {
        email = info;
    }

    public static void set_url(String URL){
        starting_url = "http:/"+URL+":3000/";
    }

    public static String get_url(){
        return starting_url;
    }

    public static void reset_cred(){
        init_devices.clear();
        init_routines.clear();
        init_consumptions_array.clear();
        init_dev_cons.clear();
        for(int i = 0; i < 5; i++){
            trend_months[i] = "";
        }
        firstname = "";
        lastname = "";
        email = "";
        id = 0;
        total_cons = 0;
        monthly_cons = 0;
    }

    public static void add_dev_cons(String Name, String App_Type, int Dev_ID, float Elec_Cons){
        init_dev_cons.add(new Home_Ellie_Devices.dev_cons(Name, App_Type, Dev_ID, Elec_Cons));
        total_cons += Elec_Cons;
    }

    public static void add_device(String Object, String desc, String app_type, String brand, String loc, int Icon, int id, int code, boolean active){
        init_devices.add(new Devices.Device(Object, desc, app_type, brand, loc, Icon, id, code, active));
    }

    public static void add_routine(String name, int id, ArrayList<Routines.Task> tasks, boolean active){
        init_routines.add(new Routines.Route(name, "" + tasks.size() + " tasks", id, tasks, active));
    }

    public static void add_trend_data(float value, int index){
        init_consumptions_array.add(new BarEntry(value, index));
    }

    public static void populate_trend_months(String starter){
        int start = 0, i;

        for(i = 0; i < month_val.length; i++){
            if(starter.matches(month_val[i])){
                start = i;
                break;
            }
        }

        for(i = 0; i < 5; i++){
            if(start + i < months.length) {
                trend_months[i] = months[start + i];
            }else{
                trend_months[i] = months[start + i - 12];
            }
        }

    }

}