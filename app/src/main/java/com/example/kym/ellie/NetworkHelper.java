package com.example.kym.ellie;

//--------------------------Holds different code for functioning with the restful API--------------------------------

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class NetworkHelper {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    //for genymotion
    //String starting_url = "http://10.0.3.2:3000/";

    //for android studio emulator
    //String starting_url = "http://10.0.2.2:3000/";

    //for rpi_local
    //String starting_url = "http://raspberrypi.local:3000/";

    //for static ip   }

    OkHttpClient client = new OkHttpClient();

    Call post(String url, String json, Callback callback) {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(user_info.get_url()+url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call delete(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(user_info.get_url()+url)
                .delete()
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call get(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(user_info.get_url()+url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    Call put(String url, String json, Callback callback) {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(user_info.get_url()+url)
                .put(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }
}